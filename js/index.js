
const baseURL2 = "https://recommendation-tool.westeurope.cloudapp.azure.com/";

const pageSize = 20;
var currentN = 0;
var cardDataAll;
var cardData;

document.addEventListener("DOMContentLoaded", () => {
    document
        .querySelectorAll('[qf-multiselect]')
        .forEach((item) => item.addEventListener('change', handleMultiSelectDropdownCB));
});

function showResult() {

    if (cardDataAll != null) {
        prepareFilter();
        return;
    }

    setButtonState("buttonShowResult", true);

    var url = baseURL2 + "dats/";

    fetch(url, { method: 'GET', mode: 'cors', headers: { 'Accept': 'application/json' }, })
        .then(function (response) {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
        .then(function (data) {
            cardDataAll = data["@graph"];
            prepareFilter();
        })
        .catch(function (error) {
            showErr("cardContainer", error);
        })
        .finally(function () {
            setButtonState("buttonShowResult", false);
        });
}

function getFilterIDs(par) {

    setButtonState("buttonShowResult", true);

    var url = baseURL2 + "recommend-with-AND-filter/";

    fetch(url, { method: 'POST', mode: 'cors', headers: { 'Accept': 'application/json' }, body: JSON.stringify(par) })
        .then(function (response) {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
        .then(function (data) {
            cardData = filterDATs(data);
            populateCards();
        })
        .catch(function (error) {
            showErr("cardContainer", error);
        })
        .finally(function () {
            setButtonState("buttonShowResult", false);
        });
}

function prepareFilter() {
    currentN = 0;
    var f;
    var s = ""; //�� ���� � ���������: document.getElementById("datText").value.toLowerCase();
    //var se = getSelectedItemsAsArray("datSector");
    //var fu = getSelectedItemsAsArray("datFunctionality");
    //var be = getSelectedItemsAsArray("datBenefits");
    //var df = getSelectedItemsAsArray("datDigitalform");
    //var la = getSelectedItemsAsArray("datLanguage");

    //�� ���� � ���������
    //if (s != "")
    //    f = cardDataAll.filter(function (item) {
    //        return item["rdfs:label"].toLowerCase().includes(s) ||
    //            item["rdfs:comment"].toLowerCase().includes(s);
    //    });

    //if (se.length === 0 && fu.length === 0 && be.length === 0 && df.length === 0 && la.length === 0) {
    if (true) {
        cardData = cardDataAll;
        populateCards();
    }
    else {
        const par = {
            "qcsm:isTargetedTowardsAgriculturalSector": se,
            "qcsm:hasFunctionality": fu,
            "qcsm:hasBenefits": be,
            "qcsm:hasDigitalForm": df,
            "qcsm:hasSupportedLanguage": la
        };

        console.log(par);

        getFilterIDs(par);
    }

}

function filterDATs(ids) {
    var f;

    f = cardDataAll.filter(function (item) {
        return ids.includes(item["@id"].replace("qfdata:", "https://quantifarm.eu/data/"))
    });

    return f;
}

function populateCards() {
    var div = document.getElementById("cardContainer");

    if (currentN == 0) {
        div.innerHTML = "";
        div.appendChild(createTotal(cardData.length));
    }
    else
        document.getElementById("buttonLoadMore").remove();

    var el;
    var j = 0;
    for (var i = currentN; i < cardData.length; i++) {

        j++;
        if (j > pageSize) {
            el = createLoadMore();
            div.appendChild(el);
            break;
        }

        item = cardData[i];
        el = createCard(i, item["rdfs:label"], item["rdfs:comment"]);
        div.appendChild(el);

    }

    currentN += --j;
}

// Create a single card
function createCard(n, title, content) {
    var card = document.createElement("div");

    card.innerHTML = `
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">${title}</h5>
        <p class="card-text">${content}</p>
        <div id="datDetail${n}">
        <p class="card-text"><a id="datDetailButton${n}" class="btn btn-primary" onclick="createDetails(${n})">Details</a></p>
        </div>
      </div>
    </div>
  `;

    return card;
}

// Create load more button
function createLoadMore() {
    var el = document.createElement("div");

    el.innerHTML = `<div class="d-flex justify-content-center" id="buttonLoadMore">
 <button type="button" class="btn btn-primary rounded-pill" onclick="populateCards()">
 <i class="bi bi-arrow-down"></i>&nbsp;&nbsp;&nbsp; ${uistrings["load_more"]} &nbsp;&nbsp;&nbsp;<i class="bi bi-arrow-down"></i></button>
 </div>`;
   
    return el;
}

// Create total
function createTotal(n) {
    var el = document.createElement("div");

    el.innerHTML = `<div class="d-flex justify-content-left">${n} DATs found
 </div>`;
   
    return el;
}

// Create DAT details
function createDetails(n) {
    var div = "datDetail" + n;
    var btn = "datDetailButton" + n;
    setButtonState(btn, true);

    var id = cardData[n]["@id"].replace("qfdata:", "https://quantifarm.eu/data/");
    var url = baseURL2 + "dats/?dat=" + id;

    fetch(url, { method: 'GET', mode: 'cors', headers: { 'Accept': 'application/json' }, })
        .then(function (response) {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
        .then(function (data) {
            var el = document.getElementById(div);

            console.log("data", data);

            var tt  = getObjPropVal(data, "qcsm:hasTitle");
            var lyr = getObjPropVal(data, "qcsm:hasLaunchYear", "@value");
            var uyr = getObjPropVal(data, "qcsm:lastUpdateYear", "@value");
            var lng = getObjPropVal(data, "qcsm:hasSupportedLanguage");
            var fn  = getObjPropVal(data, "qcsm:hasFunctionality");
            var bn  = getObjPropVal(data, "qcsm:hasBenefits");
            var kw  = getObjPropVal(data, "qcsm:hasKeyword");
            var df  = getObjPropVal(data, "qcsm:hasDigitalForm");
            var hp  = getObjPropVal(data, "qcsm:hasWebPage", "@id");
            var se  = getObjPropVal(data, "qcsm:isTargetedTowardsAgriculturalSector");

            el.innerHTML = `
<table class="table table-sm w-auto">
<tbody>
<tr><th scope="row"></th><td style="width:100%"></td>
<tr><th class="text-nowrap" scope="row">Title</th><td>${tt}</td>
<tr><th class="text-nowrap" scope="row">Agricultural Sector</th><td>${se}</td>
<tr><th class="text-nowrap" scope="row">Functionality</th><td>${fn}</td>
<tr><th class="text-nowrap" scope="row">Benefits</th><td>${bn}</td>
<tr><th class="text-nowrap" scope="row">Keywords</th><td>${kw}</td>
<tr><th class="text-nowrap" scope="row">Digital Form</th><td>${df}</td>
<tr><th class="text-nowrap" scope="row">Languages</th><td>${lng}</td>
<tr><th class="text-nowrap" scope="row">Launch year</th><td>${lyr}</td>
<tr><th class="text-nowrap" scope="row">Update year</th><td>${uyr}</td>
<tr><th class="text-nowrap" scope="row">Link</th><td><a href="${hp}" target="_blank" class="card-link">${hp}</a></td>
</tbody>
</table>
  `;

        })
        .catch(function (error) {
            showErr(div, error);
        })
        .finally(function () {
            setButtonState(btn, false);
        });

}


