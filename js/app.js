//Common constants
const baseURL = (new RegExp(/^.*\//)).exec(window.location.href)[0] + "php/"; // "http://localhost/qf/";

const role_fa = "fa";
const role_ad = "ad";
const role_pm = "pm";
const role_xx = "xx";

const key_role = "role";
const key_usr = "usr";
const sep_MultiSelect = "; ";

const toolsMetaData = [
    {
        "key": "tool1",
        "icon": "bi bi-list-stars",
        "roles": [role_xx, role_fa, role_ad]
    },
    {
        "key": "tool2",
        "icon": "bi bi-calculator-fill",
        "roles": [role_xx, role_fa, role_ad]
    },
    {
        "key": "tool3",
        "icon": "ri-bar-chart-horizontal-fill",
        "roles": [role_xx, role_fa, role_ad]
    },
    //{
    //    "key": "tool4",
    //    "icon": "bi bi-ui-checks",
    //    "roles": [role_xx, role_ad]
    //},
    {
        "key": "tool5",
        "icon": "ri-flow-chart",
        "roles": [role_xx, role_ad]
    },
    {
        "key": "tool6",
        "icon": "bi bi-bank",
        "roles": [role_xx, role_pm]
    },

]

//multilanguage functions
//
const do_lang_translation = false; 
const lang_key = "qf-lang-key";  
 
if (do_lang_translation) {
    document.addEventListener("DOMContentLoaded", () => {
        document
            .querySelectorAll("[" + lang_key + "]")
            .forEach(translateElement);
    });
}

SetLoggedUser();

function translateElement(element) {
    const key = element.getAttribute(lang_key);
    const str = uistrings[key];

    if (str != null) {
        if (element.nodeName.toLowerCase() == "input")
            element.setAttribute("placeholder", str);
        else if (element.nodeName.toLowerCase() == "label" && element.classList.contains("dropdown-item")) //<label class="dropdown-item"> �� multiple selection dropdown
            element.childNodes[1].nodeValue = str;
        else
            element.innerText = str;
    }
}

//UI navigation functions
//
function setSidebar(x) {

    var role = localStorage.getItem(key_role);
    if (role == null) role = role_fa;

    var s = `
<li class="nav-heading">${uistrings[role + "_toolkit"]}</li>
`;

    toolsMetaData.forEach((t) => {
        if (t.roles.includes(role))
            s += `            
<li class="nav-item">
    <a class="nav-link ${(x == t.key ? "" : "collapsed")}" href="${t.key}.html">
        <i class="${t.icon}" style="font-size: 32px; font-weight: normal"></i>
        <span>${uistrings[t.key]}</span>
    </a>
</li>
`;
    });

    document.getElementById("sidebar-nav").innerHTML = s;
}

function gotoTool(role, tool) {
    localStorage.setItem(key_role, role);

    var url;
    if (tool === undefined)
        url = toolsMetaData.find(t => t.roles.includes(role)).key;
    else
        url = tool;

    url += ".html";

    window.location.href = url;
}

function SetLoggedUser(func) {
    const n1 = document.getElementById('mnuLogin');
    const n2 = document.getElementById('mnuUser');
    const n3 = document.getElementById('mnuUserName');
    const n4 = document.getElementById('mnuLogin2');
    const n5 = document.getElementById('mnuLogout2');
    const n6 = document.getElementById('mnuProfile2');

    if (!n1 || !n2 || !n3) return;

    fetch('php/api.php?c=usr', { method: 'GET', mode: 'cors'})
        .then(function (response) {
            if (response.ok)
                return response.json();
            else
                return response.text()
        })
        .then(function (data) {
            localStorage.setItem(key_usr, JSON.stringify(data));

            if (data.loggedin) {
                n1.hidden = true;
                n2.hidden = false;
                n3.innerHTML = data.user;
                if (n4) n4.hidden = true;
                if (n5) n5.hidden = false;
                if (n6) n6.hidden = false;
            }
            else {
                n1.hidden = false;
                n2.hidden = true;
                if (n4) n4.hidden = false;
                if (n5) n5.hidden = true;
                if (n6) n6.hidden = true;
            }

            if (func instanceof Function) func();
        })
        .catch(function (error) {
            console.log(error);
        })
        .finally(function () {
            ;
        });
}

//multiple selection dropdown
//
function handleMultiSelectDropdownCB(event) {
    const cb = event.target;
    const dd = cb.closest('div').querySelector('input');

    if (dd.qfSelectedItemsAsArray === undefined) dd.qfSelectedItemsAsArray = [];
    if (dd.qfSelectedValuesAsArray === undefined) dd.qfSelectedValuesAsArray = [];

    if (cb.checked) {
        dd.qfSelectedItemsAsArray.push(cb.parentElement.innerText);
        dd.qfSelectedValuesAsArray.push(cb.value);
        cb.parentElement.style.fontWeight = "bold";
    } else {
        dd.qfSelectedItemsAsArray = dd.qfSelectedItemsAsArray.filter((item) => item !== cb.parentElement.innerText);
        dd.qfSelectedValuesAsArray = dd.qfSelectedValuesAsArray.filter((item) => item !== cb.value);
        cb.parentElement.style.fontWeight = "normal";
    }

    dd.value = dd.qfSelectedItemsAsArray.length > 0 ? dd.qfSelectedItemsAsArray.join(sep_MultiSelect) : '';
}

//common functions
//
function showErr(node, err) {
    console.log('Error:', err.message);

    const n = document.getElementById(node);
    n.innerHTML = "";
    const e = document.createElement("div");
    n.appendChild(e);

    e.outerHTML = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
<i class="bi bi-exclamation-octagon me-1"></i>
[${err.name}] ${err.message}
<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>`;

}

function showInfo(node, txt) {
    const n = document.getElementById(node);

    n.innerHTML = `<div class="alert alert-info alert-dismissible fade show" role="alert">
<i class="bi bi-info-square me-2"></i>${txt}
<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>`;
}

function showSpinner(node) {
    const n = document.getElementById(node);
    n.innerHTML = `<div class="d-flex justify-content-center">
    <div class="spinner-border text-primary me-3" role="status"></div>Loading...
</div>`;
}

function setButtonState(node, busy) {
    const n = document.getElementById(node);
    if (n == null) return;

    n.disabled = busy;

    if (busy) {
        var s = document.createElement("span");
        n.appendChild(s);
        s.outerHTML = `<span> ... </span><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`;
    }
    else {
        n.removeChild(n.lastElementChild);
        n.removeChild(n.lastElementChild);
    }
}

function getObjPropVal(obj, prop1, prop2 = "") {
    var v = "";
    const sep = "; ";

    if (Array.isArray(obj)) {
        v = [];
        obj.forEach((o) => { v.push(getObjPropVal(o, prop1, prop2)); });
    }
    else if (prop1 in obj) {
        if (prop2 != "" && prop2 in obj[prop1])
            v = obj[prop1][prop2];
        else
            v = obj[prop1];
    }

    if (Array.isArray(v))
        return v.join(sep);
    else
        return v;
}

function getSelectedText(selectID) {
    const el = document.getElementById(selectID);
    var t = "";

    if (el.selectedIndex >= 0)
        t = el.options[el.selectedIndex].text;

    return t;
}

function getSelectedItemsAsArray(selectID) {
    const el = document.getElementById(selectID);

    if (el.qfSelectedItemsAsArray === undefined)
        return [];
    else
        return el.qfSelectedItemsAsArray;
}

function getSelectedValuesAsArray(selectID) {
    const el = document.getElementById(selectID);

    if (el.qfSelectedValuesAsArray === undefined)
        return [];
    else
        return el.qfSelectedValuesAsArray;
}

function getUIValue(ctrl) {
//input type = "number"
//input type = "text"
//input type = "date"
//input type = "time"
//textarea
//select
//multi select: <input type="text" data-bs-toggle="dropdown"> ...

    const el = (ctrl instanceof Object) ? ctrl : document.getElementById(ctrl);
    const n = el.nodeName.toLowerCase(); 
    var v;

    if (n === "input") {
        if (el.getAttribute("data-bs-toggle") === "dropdown") {
            if (el.qfSelectedValuesAsArray)
                v = el.qfSelectedValuesAsArray.length > 0 ? el.qfSelectedValuesAsArray.join(',') : '';
        }
        else if (el.type == "number") {
            v = parseFloat(el.value);
            if (isNaN(v)) v = null;
        }
        else
            v = el.value;
    }
    else if (n === "textarea") {
        v = el.value;
    }
    else if (n === "select") {
        v = el.value;
    }
    else 
        ;

    return v;
}

function setUIValue(ctrl, v) {
    const el = (ctrl instanceof Object) ? ctrl : document.getElementById(ctrl);
    if (!el) return;

    const n = el.nodeName.toLowerCase(); 
    v = ifNull(v);

    if (n === "input") {
        if (el.getAttribute("data-bs-toggle") === "dropdown") {
            const values = v.split(sep_MultiSelect);
            const chk = Array.from(document.querySelector(`#${ctrl} ~ ul`).getElementsByTagName('input'));

            chk.forEach((item) => {
                const b = values.includes(item.value);
                const f = item.checked != b;
                item.checked = b;
                if (f) item.dispatchEvent(new Event('change', { bubbles: true }));
            });
        }
        else
            el.value = v;
    }
    else if (n === "textarea") {
        el.value = v;
    }
    else if (n === "select") {
        el.value = v;
    }
    else if (n === "td") {
        el.innerHTML = v;
    }
    else 
        ;

}

function getCheckListValues(ctrl) {
    const el = document.getElementById(ctrl);
    const chk = Array.from(el.getElementsByTagName('input'));
    var arr = [];

    chk.forEach((item) => {
        if (item.checked)
            arr.push(item.nextElementSibling.innerHTML);
        });

    return arr;
}

function setDisabled(el, v) {

    document.getElementById(el).querySelectorAll("input, button").forEach((e) => { e.disabled = v; });

}

function PostgisCoord(text) {
    // regular expression to match groups enclosed in parentheses
    const regex = /\(([^)]+)\)/g;

    const groups = [...text.matchAll(regex)];

    // extract pairs of numbers from each matched group
    const pairList = groups.map(group => {
        const groupText = group[1]; 
        const groupPairs = groupText.match(/[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?/g);

        // extract pairs from the list (assuming even number of elements)
        const pairGroup = [];
        for (let i = 0; i < groupPairs.length; i += 2) {
            pairGroup.push([Number(groupPairs[i + 1]), Number(groupPairs[i])]);
        }
        return pairGroup;
    });

    return pairList;
}

function CoordPostgis(coord) {
    var s = "";

    coord.forEach((p) => { s += p.lng + " " + p.lat + "," });

    if (s) s = "POLYGON((" + s + coord[0].lng + " " + coord[0].lat + "))"; //add first point at the end

    return s;
}

function DateHM(d) {
    return ifNull(d).trim().substring(0, 16).replace("T", " ");
}

function ifNull(v, n = "") {
    if (v === 0)
        return 0;
    else
        return v || n;
}

function ifEmpty(v, n = null) {
    if (v === 0)
        return 0;
    else
        return v ? v : n;
}

function CloneObject(o) {
    return JSON.parse(JSON.stringify(o));
}

function IsObjectEmpty(o) {
    return !o || (Object.keys(o).length === 0)
}

function FindByProp(data, findName, findValue, returnName) {

    n = data.findIndex((item) => {
        return item[findName] == findValue;
    });

    if (n >= 0)
        return data[n][returnName];
    else
        return "";
}

function FindIndexByProp(data, findName, findValue) {

    n = data.findIndex((item) => {
        return item[findName] == findValue;
    });

    return n;
}

function JoinProp(data, propName, sep) {

    if (!sep) sep = ',';

    return data.map((item) => [item[propName]]).join(sep);
}

function PopulateSelect(el, data, propText, propValue, emptyItem) {
    var e = document.getElementById(el);
    var html = "";

    if (emptyItem) html = "<option></option>";      //<option selected="" disabled="" value=""></option>
    data.forEach((item) => {
        if (propValue)
            html += `<option value="${item[propValue]}">${item[propText].substring(0, 100).trim()}</option>`;
        else if (propText)
            html += `<option>${item[propText]}</option>`;
        else
            html += `<option>${item}</option>`;
    });

    e.innerHTML = html;
}

function PopulateMultiSelect(el, data, propText, propValue) {
    setUIValue(el, "");
    var e = document.querySelector(`#${el} ~ ul`);
    var html = "";

    data.forEach((item) => {
        var v, t;

        if (propValue) {
            v = item[propValue];
            t = item[propText];
        } else if (propText) {
            v = item[propText];
            t = item[propText];
        } else {
            v = item;
            t = item;
        }

        html += `<li><label class="dropdown-item"><input type="checkbox" class="me-2 qf-scale-14" value="${v}">${t.substring(0, 100).trim()}</label></li>`;
    });

    e.innerHTML = html;
}

function PopulateCheckList(el, data, propText, propAdd, maxFirst = 5, nonZero = true) {
    var e = document.getElementById(el);
    var html = "";
    var add = "";
    var chk = "";
    var i = 0;

    data.forEach((item) => {
        if (nonZero && !item.checked && propAdd && item[propAdd] == 0)
            return;

        if (i == maxFirst) {
            html += `<div class="text-primary mt-1" onclick="ToggleViewMore(event)" data-bs-toggle="collapse" href="#collapse${el}" aria-expanded="false" role="button" aria-controls="collapse${el}">
<i class="bi bi-caret-down-fill me-1"></i><span>View more</span>
<i class="bi bi-caret-up-fill me-1" hidden></i><span hidden>View less</span>
</div>`;
            html += `<div class="collapse mb-2 mt-1" id="collapse${el}">`;
        }

        chk = (item.checked) ? "checked" : "";

        if (propAdd)
            add = `<span class="ms-2 text-black-50">(${item[propAdd]})</span>`;

        html += `<label class="qf-cursor-pointer"><input type="checkbox" class="me-2 qf-cursor-pointer" ${chk}><span>${item[propText]}</span>${add}</label><br>`;
        i++;
    });

    if (i === 0)
        html = `<span class="fst-italic ms-3 text-black-50">(no items)</span>`;

    if (i >= maxFirst)
        html += `</div>`;

    e.innerHTML = html;
}

function ToggleViewMore(event) {
    const el = event.target.getAttribute("data-bs-toggle") === "collapse" ? event.target : event.target.parentElement;

    for (let i = 0; i < el.children.length; i++) {
        el.children[i].hidden = !el.children[i].hidden;
    }
}

function CloseModal(id) {
    var m = bootstrap.Modal.getInstance(document.getElementById(id));
    m.hide();
}

function ShowModal(id) {
    var m = bootstrap.Modal.getOrCreateInstance(document.getElementById(id));
    m.show();
}

function ShowModalInfo(v) {
    const div = document.getElementById("divModalInfo");
    div.innerHTML = uistrings["info_" + v];

    ShowModal('modalInfo');
}

function GetQueryParams() {
    return new URLSearchParams(location.search);
}

//�������� �� �� ������� ���� alert �� 5 ���?
function CallAPI(url, par, funcSuccess, errContainer, startButton) {

    if (startButton != "") setButtonState(startButton, true);

    fetch(url, { method: 'POST', mode: 'cors', headers: { 'Accept': 'application/json' }, body: JSON.stringify(par) })
        .then(function (response) {
            if (response.ok)
                return response.json();
            else
                return response.text();
        })
        .then(function (data) {
            if (data instanceof Object) {
                if (funcSuccess instanceof Function) funcSuccess(data);
            }
            else
                throw new Error(data.substring(0, 100));
        })
        .catch(function (error) {
            if (errContainer != "") showErr(errContainer, error);
            console.log(error);
        })
        .finally(function () {
            if (startButton != "") setButtonState(startButton, false);
        });
}

function LoadLanguageContent(lng) {
    let href = window.location.href.trim();

    if (href.indexOf(".html") < 0) href += "index.html";

    const name = href.substring(href.lastIndexOf('/') + 1);  
    const url = href.replace(name, "assets/lng/" + name).replace(".html", "-" + lng + ".html");  
    const container = "lngTab" + lng;

    //console.log(window.location.href, url);
    if (!document.getElementById(container).innerHTML.trim() === "") return;

    fetch(url, { method: 'GET' })
        .then(function (response) {
            if (response.ok)
                return response.text();
            else
                return response.statusText;
        })
        .then(function (data) {
            document.getElementById(container).innerHTML = data;
        })
        .catch(function (error) {
            showErr(container, error);
            console.log(error);
        })
}
