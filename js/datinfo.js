const datURL = baseURL + "tool1.php";

function LoadDATDetals(id) {
    var par;

    if (!id) return;

    par = {
        "path": "/show_main_dat/",
        "data": { "dat_uri": id }
    };

    console.log("POST /show_main_dat/ body:", par.data);

    CallAPI(datURL, par, (data) => { console.log(data); CreateDetails(data.DAT_info); ShowModal('modalDAT'); }, "", "");

    return false;
}

function CreateDetails(data) {
    var el = document.getElementById("divDatDetail");
    var na = document.getElementById("modalDATTitle");

    var la =  getObjPropVal(data.description, "name");
    var de =  getObjPropVal(data.description, "description");
    var tt =  getObjPropVal(data.description, "title");
    var kw =  getObjPropVal(data.description, "keywords");
    var lyr = getObjPropVal(data.properties, "launch_year");
    var uyr = getObjPropVal(data.properties, "update_year");
    var hp =  getObjPropVal(data.properties, "web_page");
    var lng = getObjPropVal(data.filterable_properties, "languages");
    var co =  getObjPropVal(data.filterable_properties, "countries");
    var fn =  getObjPropVal(data.filterable_properties, "functionalities");
    var bn =  getObjPropVal(data.filterable_properties, "benefits");
    var df =  getObjPropVal(data.filterable_properties, "digital_forms");
    var cs =  getObjPropVal(data.filterable_properties, "cost_structures");
    var se =  getObjPropVal(data.filterable_properties, "agricultural_sectors");

    na.innerHTML = `<i class="bi bi-gear me-2"></i>${la}`;

    var s = `<div class="card-body">
    <p class="card-text">${tt}</p>
    <p class="card-text">${de}</p>
    <div><table class="table table-sm w-auto"><tbody>
        <tr><th scope="row"></th><td style="width:100%"></td></tr>`;

    if (se) s += `<tr><th class="text-nowrap" scope="row">Agricultural Sector</th><td>${se}</td></tr>`;
    if (fn) s += `<tr><th class="text-nowrap" scope="row">Functionality</th><td>${fn}</td></tr>`;
    if (bn) s += `<tr><th class="text-nowrap" scope="row">Benefits</th><td>${bn}</td></tr>`;
    if (kw) s += `<tr><th class="text-nowrap" scope="row">Keywords</th><td>${kw}</td></tr>`;
    if (df) s += `<tr><th class="text-nowrap" scope="row">Digital Form</th><td>${df}</td></tr>`;
    if (cs) s += `<tr><th class="text-nowrap" scope="row">Cost structure</th><td>${cs}</td></tr>`;
    if (lng) s += `<tr><th class="text-nowrap" scope="row">Languages</th><td>${lng}</td></tr>`;
    if (co) s += `<tr><th class="text-nowrap" scope="row">Country</th><td>${co}</td></tr>`;
    if (lyr) s += `<tr><th class="text-nowrap" scope="row">Launch year</th><td>${lyr}</td></tr>`;
    if (uyr) s += `<tr><th class="text-nowrap" scope="row">Update year</th><td>${uyr}</td></tr>`;
    if (hp) s += `<tr><th class="text-nowrap" scope="row">Link</th><td><a href="${hp}" target="_blank" class="card-link">${hp}</a></td></tr>`;

    s += `</tbody></table></div></div>`;

    el.innerHTML = s;
}
