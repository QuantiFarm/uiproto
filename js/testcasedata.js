
const testCaseData = [
    {
        id: 1,
        sector: "Arable",
        crop: "Potatoes",
        dats: [{ name:"gaiasense", id: "https://quantifarm.eu/data/fairshare/dats/5ebc05c719e27f76c9c1fcb3"}],
        country: "Greece",
        partner: "NP",
        cultivation_type: "Arable",
        comparisons: 4
    },
    {
        id: 2,
        sector: "Arable",
        crop: "Corn",
        dats: [{ name: "Irristrat", id: "https://quantifarm.eu/data/fairshare/dats/5fd1f95f3aa62c0b9acfab47" }],
        country: "Portugal",
        partner: "Agromais",
        cultivation_type: "Arable",
        comparisons: 2
    },
    {
        id: 3,
        sector: "Arable",
        crop: "Barley, wheat",
        dats: [{ name: "SATIVUM", id: "https://quantifarm.eu/data/testcase/dats/sativum" }],
        country: "Spain",
        partner: "ITACyL",
        cultivation_type: "Arable",
        comparisons: 13
    },
    {
        id: 4,
        sector: "Arable",
        crop: "Cotton",
        dats: [{ name: "Augmenta HA VRA", id: "https://quantifarm.eu/data/testcase/dats/augmenta_ha_vra" }],
        country: "Greece",
        partner: "Augmenta",
        cultivation_type: "Arable",
        comparisons: 6
    },
    {
        id: 5,
        sector: "Arable",
        crop: "Wheat",
        dats: [{ name: "granoduro.net", id: "https://quantifarm.eu/data/testcase/dats/granoduro.net" }],
        country: "Turkiye",
        partner: "HORTA",
        cultivation_type: "Arable",
        comparisons: 10
    },
    {
        id: 6,
        sector: "Arable",
        crop: "Wheat, onion, potato",
        dats: [{ name: "Delphy QMS system", id: "https://quantifarm.eu/data/testcase/dats/delphy_qms_system" }, { name: "Agrovision", id: "https://quantifarm.eu/data/testcase/dats/agrovision" }, { name: "Aurea Imaging", id: "https://quantifarm.eu/data/testcase/dats/aurea_imaging" }, { name: "Agro exact sencrop", id: "https://quantifarm.eu/data/testcase/dats/agro_exact_sencrop" }, { name: "Raven", id: "https://quantifarm.eu/data/testcase/dats/raven" }],
        country: "Netherlands",
        partner: "Delphy",
        cultivation_type: "Arable",
        comparisons: 1
    },
    {
        id: 7,
        sector: "Arable",
        crop: "Potatoes",
        dats: [{ name: "gaiasense", id: "https://quantifarm.eu/data/fairshare/dats/5ebc05c719e27f76c9c1fcb3" }],
        country: "Poland",
        partner: "FFP2",
        cultivation_type: "Arable",
        comparisons: 3
    },
    {
        id: 8,
        sector: "Arable",
        crop: "Wheat",
        dats: [{ name: "AgroSmart for Silos", id: "https://quantifarm.eu/data/testcase/dats/agrosmart_for_silos" }],
        country: "Latvia",
        partner: "AgroSmart",
        cultivation_type: "Silos",
        comparisons: 1
    },
    {
        id: 9,
        sector: "Arable",
        crop: "Barley, corn, wheat",
        dats: [{ name: "Farm manager", id: "https://quantifarm.eu/data/testcase/dats/farm_manager" }],
        country: "Slovenia",
        partner: "KGZS",
        cultivation_type: "Arable",
        comparisons: 4
    },
    {
        id: 10,
        sector: "Arable",
        crop: "Wheat",
        dats: [{ name: "SF DSS", id: "https://quantifarm.eu/data/testcase/dats/sf_dss" }],
        country: "Romania",
        partner: "ANAMOB",
        cultivation_type: "Arable",
        comparisons: 1
    },
    {
        id: 11,
        sector: "Fruit",
        crop: "Olives",
        dats: [{ name: "gaiasense", id: "https://quantifarm.eu/data/fairshare/dats/5ebc05c719e27f76c9c1fcb3" }],
        country: "Greece",
        partner: "NP",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 12,
        sector: "Fruit",
        crop: "Apples",
        dats: [{ name: "Delphy QMS system", id: "https://quantifarm.eu/data/testcase/dats/delphy_qms_system" }, { name: "RIMpro", id: "https://quantifarm.eu/data/testcase/dats/rimpro" }, { name: "Trapview", id: "https://quantifarm.eu/data/testcase/dats/trapview" }, { name: "Aurea Imaging", id: "https://quantifarm.eu/data/testcase/dats/aurea_imaging" }, { name: "Estede", id: "https://quantifarm.eu/data/testcase/dats/estede" }],
        country: "Poland",
        partner: "Delphy",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 13,
        sector: "Fruit",
        crop: "Grapes",
        dats: [{ name: "Vite.net", id: "https://quantifarm.eu/data/testcase/dats/vite.net" }],
        country: "Italy",
        partner: "HORTA",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 14,
        sector: "Fruit",
        crop: "Strawberries, Blueberry",
        dats: [{ name: "NETAJET", id: "https://quantifarm.eu/data/testcase/dats/netajet" }, { name: "NMC pro system", id: "https://quantifarm.eu/data/testcase/dats/nmc_pro_system" }],
        country: "Serbia",
        partner: "Terra",
        cultivation_type: "Greenhouse", //Horticulture
        comparisons: 1
    },
    {
        id: 15,
        sector: "Fruit",
        crop: "Olives",
        dats: [{ name: "gaiasense", id: "https://quantifarm.eu/data/fairshare/dats/5ebc05c719e27f76c9c1fcb3" }],
        country: "Cyprus",
        partner: "Filagro",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 16,
        sector: "Fruit",
        crop: "Apples",
        dats: [{ name: "QMS Water Software", id: "https://quantifarm.eu/data/testcase/dats/qms_water_software" }, { name: "RIMpro", id: "https://quantifarm.eu/data/testcase/dats/rimpro" }, { name: "Trapview", id: "https://quantifarm.eu/data/testcase/dats/trapview" }, { name: "Aurea Imaging", id: "https://quantifarm.eu/data/testcase/dats/aurea_imaging" }, { name: "Estede", id: "https://quantifarm.eu/data/testcase/dats/estede" }],
        country: "Netherlands",
        partner: "Delphy",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 17,
        sector: "Fruit",
        crop: "Grapes",
        dats: [{ name: "fms.agricloud.ro", id: "https://quantifarm.eu/data/testcase/dats/fms.agricloud.ro" }],
        country: "Romania",
        partner: "ANAMOB",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 18,
        sector: "Vegetables",
        crop: "Tomatoes",
        dats: [{ name: "pomodoro.net", id: "https://quantifarm.eu/data/testcase/dats/pomodoro.net" }],
        country: "Italy",
        partner: "HORTA",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 19,
        sector: "Vegetables",
        crop: "Tomatoes",
        dats: [{ name: "QMS Tomatos (Delphy)", id: "https://quantifarm.eu/data/testcase/dats/qms_tomatos_(delphy)" }],
        country: "Netherlands",
        partner: "Delphy",
        cultivation_type: "Greenhouse", //Horticulture
        comparisons: 1
    },
    {
        id: 20,
        sector: "Fruit",
        crop: "Bananas / Grapes",
        dats: [{ name: "Agroptima", id: "https://quantifarm.eu/data/fairshare/dats/5e32198850aeb3490a24f39f" }, { name: "NADIA", id: "https://quantifarm.eu/data/testcase/dats/nadia" }],
        country: "Canary Islands",
        partner: "Anysol",
        cultivation_type: "Horticulture",
        comparisons: 1
    },
    {
        id: 21,
        sector: "Vegetables",
        crop: "Tomatoes",
        dats: [{ name: "Signum", id: "https://quantifarm.eu/data/testcase/dats/signum" }, { name: "Kathari UF1", id: "https://quantifarm.eu/data/testcase/dats/kathari_uf1" }],
        country: "Finland",
        partner: "LUKE",
        cultivation_type: "Greenhouse", //Horticulture
        comparisons: 1
    },
    {
        id: 22,
        sector: "Meat",
        crop: "Poultry",
        dats: [{ name: "NetFLOX", id: "https://quantifarm.eu/data/testcase/dats/netflox" }],
        country: "UK",
        partner: "FLOX",
        cultivation_type: "Meat-poultry",
        comparisons: 1
    },
    {
        id: 23,
        sector: "Meat",
        crop: "Cows",
        dats: [{ name: "Lely", id: "https://quantifarm.eu/data/testcase/dats/lely" }, { name: "MSD & Lely", id: "https://quantifarm.eu/data/testcase/dats/msd_&_lely" }, { name: "Innoval", id: "https://quantifarm.eu/data/testcase/dats/innoval" }],
        country: "France",
        partner: "IDELE",
        cultivation_type: "Meat-cow",
        comparisons: 1
    },
    {
        id: 24,
        sector: "Meat",
        crop: "Pigs",
        dats: [{ name: "PigUp", id: "https://quantifarm.eu/data/testcase/dats/pigup" }, { name: "Acerva", id: "https://quantifarm.eu/data/testcase/dats/acerva" }],
        country: "Belgium",
        partner: "Leuven",
        cultivation_type: "Meat-pig",
        comparisons: 1
    },
    {
        id: 25,
        sector: "Dairy",
        crop: "Cows",
        dats: [{ name: "Allflex Livestock Intelligence", id: "https://quantifarm.eu/data/testcase/dats/allflex_livestock_intelligence" }, { name: "Lely", id: "https://quantifarm.eu/data/testcase/dats/lely" }],
        country: "France",
        partner: "IDELE",
        cultivation_type: "Milk",
        comparisons: 1
    },
    {
        id: 26,
        sector: "Dairy",
        crop: "Cows",
        dats: [{ name: "DeLaval Dairy Services", id: "https://quantifarm.eu/data/testcase/dats/delaval_dairy_services" }],
        country: "Ireland",
        partner: "Teagasc",
        cultivation_type: "Milk",
        comparisons: 1
    },
    {
        id: 27,
        sector: "Dairy",
        crop: "Cows",
        dats: [{ name: "?", id: "https://quantifarm.eu/data/" }],
        country: "Germany",
        partner: "Leuven",
        cultivation_type: "Milk",
        comparisons: 1
    },
    {
        id: 28,
        sector: "Dairy",
        crop: "Cows",
        dats: [{ name: "Boumatic milking", id: "https://quantifarm.eu/data/testcase/dats/boumatic_milking" }],
        country: "Romania",
        partner: "ANAMOB",
        cultivation_type: "Milk",
        comparisons: 1
    },
    {
        id: 29,
        sector: "Apiculture",
        crop: "Bees",
        dats: [{ name: "ART 21", id: "https://quantifarm.eu/data/testcase/dats/art_21" }],
        country: "Lithuania",
        partner: "ART21",
        cultivation_type: "Bees",
        comparisons: 1
    },
    {
        id: 30,
        sector: "Aquaculture",
        crop: "Oyster",
        dats: [{ name: "BENCO", id: "https://quantifarm.eu/data/testcase/dats/benco" }],
        country: "Croatia",
        partner: "Benco",
        cultivation_type: "Oyster",
        comparisons: 1
    }
];

const countryData = [
    {
        id: "Belgium",
        icon: "belgium.png",
        latlng: [50.72254683363231, 4.257202148437501]
    },
    {
        id: "Canary Islands",
        icon: "canary.png",
        latlng: [30.315987718557867, -17.369384765625004]
    },
    {
        id: "Croatia",
        icon: "croatia.png",
        latlng: [44.15068115978094, 15.172119140625002]
    },
    {
        id: "Cyprus",
        icon: "cyprus.png",
        latlng: [35.191766965947394, 32.66510009765626]
    },
    {
        id: "Finland",
        icon: "finland.png",
        latlng: [62.71446210149774, 23.181152343750004]
    },
    {
        id: "France",
        icon: "france.png",
        latlng: [45.98169518512228, 1.4282226562500002]
    },
    {
        id: "Germany",
        icon: "germany.png",
        latlng: [52.03897658307622, 11.019287109375]
    },
    {
        id: "Greece",
        icon: "greece.png",
        latlng: [37.70120736474139, 21.829833984375004]
    },
    {
        id: "Ireland",
        icon: "ireland.png",
        latlng: [52.8823912222619, -9.503173828125]
    },
    {
        id: "Italy",
        icon: "italy.png",
        latlng: [42.032974332441405, 12.008056640625]
    },
    {
        id: "Latvia",
        icon: "latvia.png",
        latlng: [57.58655886615978, 24.664306640625]
    },
    {
        id: "Lithuania",
        icon: "lithuania.png",
        latlng: [55.751849391735284, 22.818603515625]
    },
    {
        id: "Netherlands",
        icon: "netherlands.png",
        latlng: [53.199451902831555, 5.350341796875001]
    },
    {
        id: "Poland",
        icon: "poland.png",
        latlng: [53.067626642387374, 18.522949218750004]
    },
    {
        id: "Portugal",
        icon: "portugal.png",
        latlng: [40.730608477796636, -8.865966796875002]
    },
    {
        id: "Romania",
        icon: "romania.png",
        latlng: [46.89023157359399, 24.411621093750004]
    },
    {
        id: "Serbia",
        icon: "serbia.png",
        latlng: [44.18220395771566, 20.511474609375004]
    },
    {
        id: "Slovenia",
        icon: "slovenia.png",
        latlng: [46.255846818480315, 14.683227539062502]
    },
    {
        id: "Spain",
        icon: "spain.png",
        latlng: [40.413496049701955, -2.5158691406250004]
    },
    {
        id: "Turkiye",
        icon: "turkiye.png",
        latlng: [40.54720023441049, 32.71179199218751]
    },
    {
        id: "UK",
        icon: "uk.png",
        latlng: [55.33539361201609, -3.4881591796875004]
    }
];

const fieldsData = [
    { field: "testcaseNumber", disable: [] },
    { field: "testcaseLeader", disable: [] },
    { field: "cultivationType", disable: [] },
    { field: "datsInformation", disable: [] },
    { field: "farmNumber", disable: [] },
    { field: "biogeographicalRegion", disable: ["Silos"] },
    { field: "totalFarmSize", disable: ["Silos"] },
    { field: "parcelName", disable: [] },
    { field: "parcelDimension", disable: [] },
    { field: "soilFractionSand", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "soilFractionClay", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "soilFractionSilt", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "bulkDensity", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "soilOrganicCarbon", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "numberOfAnimals", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "numberOfPlants", disable: ["Arable", "Horticulture", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "livestockUnits", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "numberOfBeehives", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Silos"] },
    { field: "aquacultureArea", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "datsUsed", disable: [] },
    { field: "cultivationYear", disable: [] },
    { field: "startMonth", disable: ["Silos"] },
    { field: "endMonth", disable: ["Silos"] },
    { field: "productType", disable: [] },
    { field: "numberOfEmployees", disable: [] },
    { field: "numberOfFulltimeEmployees", disable: [] },
    { field: "numberOfSeasonalEmployees", disable: [] },
    { field: "waterPrice", disable: ["Oyster", "Bees", "Silos"] },
    { field: "fuelPrice", disable: [] },
    { field: "electricityPrice", disable: ["Oyster", "Bees"] },
    { field: "averageSalary", disable: [] },
    { field: "averageProfessionalSalary", disable: [] },
    { field: "productMarketPrice", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Oyster"] },
    { field: "cropMarketPrice", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Oyster", "Silos"] },
    { field: "meatMarketPrice", disable: ["Arable", "Horticulture", "Greenhouse", "Milk", "Bees", "Oyster", "Silos"] },
    { field: "milkMarketPrice", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Bees", "Oyster", "Silos"] },
    { field: "oysterMarketPrice", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "honeyMarketPrice", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Silos"] },
    { field: "totalWaterUsage", disable: ["Oyster", "Bees", "Silos"] },
    { field: "waterUsageForIrrigation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "waterUsageForFertigation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "waterUsageForPestDilution", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "waterUsageForCleaning", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "waterUsageForDrinking", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "totalFuelUsage", disable: [] },
    { field: "fuelUsageForFertilisation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "fuelUsageForPesticides", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "fuelUsageForIrrigation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "fuelUsageForSowing", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "fuelUsageForPruning", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "fuelUsageForTemperatureHumidityControl", disable: ["Arable", "Horticulture", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "fuelUsageForBeehivesVisits", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Silos"] },
    { field: "fuelUsageForTransportingOysters", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "fuelUsageForMilking", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "fuelUsageForFeeding", disable: ["Arable", "Horticulture", "Greenhouse", "Bees", "Silos"] },
    { field: "totalElectricityUsage", disable: ["Oyster", "Bees"] },
    { field: "electricityUsageForTemperatureHumidityControl", disable: ["Arable", "Horticulture", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "electricityUsageForIrrigation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "electricityUsageForMilking", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "electricityUsageForHeatDetection", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "electricityUsageForCalvingDetection", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Oyster", "Bees", "Silos"] },
    { field: "electricityUsageForFeeding", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "fuelForTemperatureControlInformation", disable: ["Arable", "Horticulture", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "pesticidesUsageInformation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "fertilizersUsageInformation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "feedUsageInformation", disable: ["Arable", "Horticulture", "Greenhouse", "Silos"] },
    { field: "drugsUsageInformation", disable: ["Arable", "Horticulture", "Greenhouse", "Silos"] },
    { field: "labourForIrrigation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "labourForPesticides", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "labourForFertilisation", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "labourForSowingOrPlanting", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "labourForPruning", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "labourForFieldVisits", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "labourForGreenhouseManagement", disable: ["Arable", "Horticulture", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "labourForQuantityTracking", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "labourForGrainQualityAssessment", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "labourForControllingPricesAndQuantities", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "labourForOrganisingLogistics", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "labourForCleaning", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "labourForStableVisits", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "labourForHeatDetection", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "labourForCalvingDetection", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Oyster", "Bees", "Silos"] },
    { field: "labourForPigstyManagement", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "labourForMilking", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "labourForMilkQualityAssessment", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "labourForFeeding", disable: ["Arable", "Horticulture", "Greenhouse", "Oyster", "Bees", "Silos"] },
    { field: "labourForBeehiveVisits", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Silos"] },
    { field: "labourForOysterSampling", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "labourForOysterTransportation", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "labourForHarvesting", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "labourForStorage", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees"] },
    { field: "labourForAdministration", disable: [] },
    { field: "numberOfFulfilledOrders", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "numberOfOrdersReceived", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "numberOfWrongOrders", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "additionalRevenues", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "percentageProteinInDryMatter", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "humidity", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "testWeight", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "cropProduction", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "milkProduction", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "meatProduction", disable: ["Arable", "Horticulture", "Greenhouse", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "oysterProduction", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Bees", "Silos"] },
    { field: "honeyProduction", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Silos"] },
    { field: "silosProduction", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "numberOfDeathAnimals", disable: ["Arable", "Horticulture", "Greenhouse", "Silos"] },
    { field: "calvesDiedInFirst24Hours", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Oyster", "Bees", "Silos"] },
    { field: "totalNumberOfCalvesBorn", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Oyster", "Bees", "Silos"] },
    { field: "numberOfDaysWithPastureAccess", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "outdoorLoafingArea", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "herdSize", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees", "Silos"] },
    { field: "numberOfCowsWithHighSCCMilk", disable: ["Arable", "Horticulture", "Greenhouse", "Meat-poultry", "Meat-pig", "Oyster", "Bees", "Silos"] },
    { field: "numberOfNewInjuryCases", disable: [] },
    { field: "totalNumberOfLostWorkingHours", disable: [] },
    { field: "totalNumberOfWorkingHours", disable: [] },
    { field: "numberOfWorkingWeeks", disable: [] },
    { field: "numberOfNewLocalEmployeesHired", disable: [] },
    { field: "numberOfNewEmployeesHired", disable: [] },
    { field: "additionalFieldsInformation", disable: [] },

    //ui fields
    { field: "_ui.waterTitle", disable: ["Oyster", "Bees", "Silos"] },
    { field: "_ui.waterTotal", disable: ["Oyster", "Bees", "Silos"] },
    { field: "_ui.waterActivity", disable: ["Oyster", "Bees", "Silos"] },
    { field: "_ui.electricityTitle", disable: ["Oyster", "Bees"] },
    { field: "_ui.electricityTotal", disable: ["Oyster", "Bees"] },
    { field: "_ui.electricityActivity", disable: ["Oyster", "Bees", "Silos"] },
    { field: "_ui.fuelActivity", disable: ["Silos"] },
    { field: "_ui.performanceTitle", disable: ["Meat-poultry", "Meat-cow", "Meat-pig", "Milk", "Oyster", "Bees"] },
    { field: "_ui.animalTitle", disable: ["Arable", "Horticulture", "Greenhouse", "Silos"] },

];

const defObj = {
    testcaseNumber: 0, testcaseLeader: "", cultivationType: "", datsInformation: [],
    yearlyAssessmentInformation: [
        {
            cultivationYear: 2023,
            parcelComparison: [{
                parcelAssessmentWithDATS: {},
                parcelAssessmentWithoutDATS: {}
            }]
        },
        {
            cultivationYear: 2024,
            parcelComparison: []
        },
        {
            cultivationYear: 2025,
            parcelComparison: []
        }
    ]
}; // default empty object

const emptyPC = { parcelAssessmentWithDATS: {}, parcelAssessmentWithoutDATS: {} };


