﻿//UI language strings
//see "multilanguage functions" section in app.js 


const uistrings = {
    //roles names
    "fa": "Farmer",
    "ad": "Advisor",
    "pm": "Policy Maker",

    //roles toolkit names
    "fa_toolkit": "Farmer Toolkit",
    "ad_toolkit": "Advisor Toolkit",
    "pm_toolkit": "Policy Maker Toolkit",
    "xx_toolkit": "All tools",

    //tools names
    "tool1": "Recommendations Tool",
    "tool1x":"Recommendations Tool",
    "tool2": "Cost and Benefit Calculators",
    "tool3": "Benchmarking Tool",
    "tool4": "Compliance Check Tool",
    "tool5": "Advanced Decision Support Tool",
    "tool6": "Policy Monitoring Tool",

    //UI strings
    "load_more": "Load more",
    "key1": "1Input data1",
    "key2": "1Select Farm1",
    "key3": "1Happy happy chickens1",
    "key4": "1Parcel area1",

    //tool2, calculators results captions
    "total_cost_of_dat_purchase": "Total cost of purchase (€)",
    "yield_increase": "Yield increase (%)",
    "current_revenuein_1_year": "Current revenue (€ in 1 year)",
    "increased_yield_tonsha": "Increased yield (tons/ha)",
    "price_of_increased_yieldha": "Price of increased yield (€/ha)",
    "increased_revenuein_1_year": "Increased revenue (€ in 1 year)",
    "fertilization_saving": "Fertilization saving (%)",
    "current_fertilization_costha": "Current fertilization (€/ha)",
    "reduced_fertilizer_usage_kgha": "Reduced fertilizer usage (kg/ha)",
    "fertilizer_cost_savingsha": "Fertilizer cost saving (€/ha)",
    "fertilization_cost_savingin_1_year": "Fertilization cost saving (€ in 1 year)",
    "water_saving": "Water saving (%)",
    "current_irrigation_costha": "Current irrigation cost (€/ha)",
    "reduced_water_usage_m3ha": "Reduced water usage (m3/ha)",
    "water_cost_savings_ha": "Water cost saving (€/ha)",
    "water_cost_savings_in_1_year": "Water cost saving (€ in 1 year)",
    "pesticide_saving": "Pesticide saving (%)",
    "current_pesticide_costha": "Current pesticide cost (€/ha)",
    "reduced_pesticide_usage_kg_or_lt_ha": "Reduced pesticide usage (kg or lt/ha)",
    "pesticide_cost_savingha": "Pesticide cost saving (€/ha)",
    "pesticide_cost_savingsin_1_year": "Pesticide cost saving (€ in 1 year)",
    "labor_saving": "Labor saving (%)",
    "labor_cost_savings_in_1_year": "Labor cost saving (€ in 1 year)",
    "fuel_saving": "Fuel saving (%)",
    "fuel_cost_savings_in_1_year": "Fuel cost saving (€ in 1 year)",
    "costs_saving": "Costs saving (€)",
    "return_on_investment": "Return on investment (€)",
    "net_benefit": "Net benefit (€)",

    //tool2, info
    "info_return_on_investment": "<p>Return on Investment (ROI) is a financial metric used to evaluate the efficiency and profitability of your investment in Digital Agricultural Technologies (DATs). Expressed as a percentage, the ROI figure indicates the return you can expect on every dollar invested into a DAT, after accounting for the costs of the investment.</p><p>A positive ROI means you are gaining more value from the DAT than it costs to implement. For example, an ROI of 20% suggests that for every $1 invested, you're getting $1.20 back in returns. On the other hand, a negative ROI indicates a loss, meaning the cost of the DAT outweighs the benefits it provides to your farming operations.</p><p>Understanding the ROI of a DAT can help you make informed decisions about which technologies are most likely to improve your farm's profitability.</p>",
    "info_net_benefit": "<p>Net Benefit is a straightforward measure that calculates the total financial gain (or loss) from investing in a Digital Agricultural Technology (DAT), after all costs of the investment have been subtracted. It provides a dollar value that represents the net increase (or decrease) in your farm's income attributable to the DAT.</p><p>The Net Benefit takes into account all relevant costs, including the initial purchase, installation, maintenance, and any other associated expenses, and subtracts these from the total benefits derived from using the DAT. Benefits can include increased yields, reduced labor costs, improved product quality, and other financial gains.</p><p>A positive Net Benefit means that the technology adds more value to your farm than it costs, highlighting a financially beneficial investment. Conversely, a negative Net Benefit indicates that the costs of implementing the DAT exceed the financial gains, suggesting it might not be a viable investment for your operations.</p><p>By evaluating the Net Benefit of a DAT, you can gauge its overall impact on your farm's financial health and make more informed decisions about which technologies to adopt.</p>"


};