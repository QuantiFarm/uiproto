const tool_key = "tool2";
const apiURL = baseURL + "tool2.php";

const crop = "crop";
const live = "livestock";

var currentFarmType = "";
var dataDATs;
var dataTree = {
    crop: {},
    livestock: {}
};
var dataUserData = {
    crop: {},
    livestock: {}
};
var currentType = {
    crop: "",
    livestock: ""
};
var currentDAT = {
    crop: "",
    livestock: ""
};

document.addEventListener("DOMContentLoaded", setSidebar(tool_key));


SetLoggedUser(SetUI);

function SetUI() {
    user = JSON.parse(localStorage.getItem(key_usr));

    if (!user.loggedin) {
        document.getElementById('lnkLogin').hidden = !true;

        document.getElementById('lockTabCrop').hidden = !true;
        document.getElementById('btnTabCrop').disabled = !false;
        document.getElementById('lockTabLive').hidden = !true;
        document.getElementById('btnTabLive').disabled = !false;

    } else
        LoadData();
}

function LoadData() {
    var par;
    const div1 = "divDAT" + live;
    const div2 = "divDAT" + crop;

    showSpinner(div1);
    showSpinner(div2);

    par = {
        "path": "/dat_tree",
        "data": { "myDatType": crop }
    };

    CallAPI(apiURL, par, (data) => { dataTree[crop] = data; OnClickTab(crop); }, div1, "");

    par = {
        "path": "/dat_tree",
        "data": { "myDatType": live }
    };

    CallAPI(apiURL, par, (data) => { dataTree[live] = data; OnClickTab(live); }, div2, "");

    par = {
        "path": "/user_data_get",
        "type": "crops",  // 's' at the end!
        "data": { user_id: "" }
    };

    CallAPI(apiURL, par, (data) => { dataUserData[crop] = FlatObject(data); PopulateUserData(crop); }, "", "");

    par = {
        "path": "/user_data_get",
        "type": live,
        "data": { user_id: "" }
    };

    CallAPI(apiURL, par, (data) => { dataUserData[live] = FlatObject(data); PopulateUserData(live); }, "", "");

    par = {
        "path": "/dat",
        "data": {}
    };

    CallAPI(apiURL, par, (data) => { dataDATs = data; }, div1, "");

}

function PopulateTree() {
    if (Object.keys(dataTree[currentFarmType]).length === 0) return;

    var tree = dataTree[currentFarmType][currentType[currentFarmType]];
    var div = document.getElementById("divDAT" + currentFarmType);
    var o1, o2;
    var i = 0;
    var pref = "tree" + currentFarmType;
    var html = `<ul id="${pref}">`;

    //hard-coded 3 levels tree
    for (var l1 in tree) {
        o1 = tree[l1];
        html += `<li id="${pref + i++}"><a>${l1}</a><ul>`;

        for (var l2 in o1) {
            o2 = o1[l2];
            html += `<li id="${pref + i++}"><a>${l2}</a><ul>`;

            for (var l3 in o2) {
                html += `<li id="${pref + i++}"><a href="#" onclick="OnClickDAT('${o2[l3]}'); return false;">${o2[l3]}</a>
<span><a href="#" onclick="ShowDetails('${o2[l3]}'); return false;" title="DAT info">
<i class="bi bi-info-circle fa-lg"></i></a></span></li>`;
            }
            html += `</ul></li>`;
        }
        html += `</ul></li>`;
    }
    html += `</ul>`;

    NavTree.remove(document.querySelector("#" + pref));

    div.innerHTML = html;

    new NavTree(document.querySelector("#" + pref), {
        searchable: false,
        showEmptyGroups: true,

        groupOpenIconClass: "fa-regular",
        groupOpenIcon: "fa-folder-open",

        groupCloseIconClass: "fa-solid",
        groupCloseIcon: "fa-folder",

        linkIconClass: "bi",
        linkIcon: "bi-gear"
    });

}

function PopulateUserData(t) {
    Object.keys(dataUserData[t]).forEach((k) => {
        setUIValue(t + "_" + k, Number(dataUserData[t][k] || 0));
    });
}

function PopulateTotalResult(data) {
    ClearTotalResult();

    Object.keys(data).forEach((k) => {
        setUIValue(currentFarmType + "_" + k, (Number(data[k])).toFixed(2));
    });
}

function ClearTotalResult() {
    Array.from(document.getElementById("divCalcs" + currentFarmType).getElementsByTagName('td')).forEach((n) => {
        if (n.id.startsWith(currentFarmType))
            n.innerHTML = "---";
    });
}

function OnClickTab(t) {
    if (Object.keys(dataTree[t]).length === 0) return;

    currentFarmType = t;
    OnChangeType();
}

function OnClickDAT(name) {
    currentDAT[currentFarmType] = name;
    setDisabled('divCalcList' + currentFarmType, false);
    PopulateUserData(currentFarmType);
    GetTotalResult();
}

function OnChangeType() {
    var t = getUIValue("cboType" + currentFarmType);

    if (t == currentType[currentFarmType]) return;

    currentType[currentFarmType] = t;
    currentDAT[currentFarmType] = "";

    PopulateTree();
    ClearTotalResult();
    setDisabled('divCalcList' + currentFarmType, true);
}

function Recalculate() {
    var newData = { user_id: "" };

    Array.from(document.getElementById("divCalcs" + currentFarmType).getElementsByTagName('input')).forEach((n) => {
        if (n.id.startsWith(currentFarmType))
            newData[n.id.slice(currentFarmType.length + 1)] = Number(n.value);
    });

    var par = {
        "path": "/user_data_post",
        "type": (currentFarmType == crop) ? "crops" : currentFarmType, // 's' at the end!
        "data": [newData]
    };

    console.log(newData);

    CallAPI(apiURL, par, (data) => { dataUserData[currentFarmType] = newData; GetTotalResult(); }, "", "btnCalc" + currentFarmType);
}

function GetTotalResult() {
    if (Object.keys(dataUserData[currentFarmType]).length === 0) return;

    var par = {
        "path": "/totals/",
        "data": {
            "user_id": "",
            "dat_name": currentDAT[currentFarmType],
            "myCultivationType": currentType[currentFarmType],
            "type": currentFarmType
       }
    };

    CallAPI(apiURL, par, (data) => { PopulateTotalResult(data.result) }, "", "btnCalc" + currentFarmType);
}

function ShowDetails(name) {
    n = FindIndexByProp(dataDATs, 'dat_name', name);
    if (n < 0) return;

    var dat = dataDATs[n];
    var el = document.getElementById("divDatDetail");
    var na = document.getElementById("modalDATTitle");

    if (dat.platform_link == "NA" || dat.platform == "NA") {
        dat.platform_link = "";
        dat.platform = "";
    }

    na.innerHTML = `<i class="bi bi-gear me-2"></i>${dat.dat_name}`;

    el.innerHTML = `
<div class="card-body">
    <p class="card-text">${dat.dat_short_description}</p>
    <div>
        <table class="table table-sm w-auto">
            <tbody>
                <tr><th scope="row"></th><td style="width:100%"></td></tr>
                <tr><th class="text-nowrap" scope="row">DAT Category</th><td>${dat.dat_category}</td></tr>
                <tr><th class="text-nowrap" scope="row">Purpose for</th><td>${dat.purpose_for}</td></tr>
                <tr><th class="text-nowrap" scope="row">Livestock / Crop type</th><td>${dat.crop_type}</td></tr>
                <tr><th class="text-nowrap" scope="row">Cost info</th><td>${dat.cost_info}</td></tr>
                <tr><th class="text-nowrap" scope="row">Provider</th><td><a href="${dat.dat_provider_website}" target="_blank" class="card-link">${dat.dat_provider_name}</a></td></tr>
                <tr><th class="text-nowrap" scope="row">Platform</th><td><a href="${dat.platform_link}" target="_blank" class="card-link">${dat.platform}</a></td></tr>
            </tbody>
        </table>
    </div>
</div>
`
    ShowModal('modalDAT');
}

function FlatObject(o) {
    var f = {};

    Object.keys(o).forEach((k) => {
        if (o[k] instanceof Object)
            Object.keys(o[k]).forEach((k1) => { f[k1] = o[k][k1] });
        else
            f[k] = o[k];
    });

    return f;
}

