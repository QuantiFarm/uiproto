const tool_key = "tool3";
const apiURL = baseURL + "tool3.php";
var mapFlag = false;
//var refMapPolygon = null;

//data
var dataParcels;
var dataDATs;
var user;

document.addEventListener("DOMContentLoaded", setSidebar(tool_key));

document.addEventListener("DOMContentLoaded", () => {
    document
        .querySelectorAll('[qf-multiselect]')
        .forEach((item) => item.addEventListener('change', handleMultiSelectDropdownCB));
});

SetLoggedUser(SetUI);


function SetUI() {
    user = JSON.parse(localStorage.getItem(key_usr));

    if (!user.loggedin) {
        document.getElementById('lnkLogin').hidden = !true;
        document.getElementById('lockParcels').hidden = !true;
        document.getElementById('lockEvents').hidden = !true;
        document.getElementById('lockMeteo').hidden = !true;
        document.getElementById('lockResults').hidden = !true;
        document.getElementById('btnTabParcels').disabled = !false;
        document.getElementById('btnTabEvents').disabled = !false;
        document.getElementById('btnTabMeteo').disabled = !false;
        document.getElementById('btnTabResult').disabled = !false;
    } else
        LoadData();
}

function LoadData() {
    LoadEnumsData();
    LoadDATsAll();
    LoadParcelsAll();
}

function SetMap() {

    if (!mapFlag) {

        map = L.map('map', { editable: true }).setView([48, 15], 4);
        
        var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '&copy; <a href="https://openstreetmap.org" target="_blank">OpenStreetMap</a>',
            id: 'OSM'
        }).addTo(map);

        var gm = L.tileLayer('https://{s}.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}', {
            maxZoom: 18,
            attribution: '&copy; <a href="https://www.google.com/maps" target=_blank>Google Maps</a>',
            subdomains: ["mt0", "mt1", "mt2", "mt3"],
            id: 'GM'
        }).addTo(map);

        L.control.layers({ "OpenStreetMap": osm, "Google maps": gm }).addTo(map);

        //map.on('editable:editing', function (event) {
        //    console.log("editing",event.layer._latlngs);
        //});

        mapFlag = true;
    }

    var coord = PostgisCoord(getUIValue("txtParcelPoly"));
    DeleteDrawing();

    if (coord != "") {

        var poly = L.polygon(coord, { color: 'red' });
        map.addLayer(poly);
        map.fitBounds(poly.getBounds().pad(0.1));

    }
    else {
        map.setView([48, 15], 4);

    }
}

function StartDrawing() {
    var poly;

    map.editTools.stopDrawing();
    map.eachLayer(function (layer) { if (layer instanceof L.Polygon) poly = layer; });

    if (poly)
        poly.enableEdit();
    else
        map.editTools.startPolygon("", { color: 'red' });
}

function FinishDrawing() {
    var poly;

    map.editTools.commitDrawing();
    map.eachLayer(function (layer) { if (layer instanceof L.Polygon) poly = layer; });

    if (poly) {
        setUIValue("txtParcelPoly", CoordPostgis(poly._latlngs[0]));
        poly.disableEdit();
    }
}

function DeleteDrawing() {
    map.editTools.stopDrawing();
    map.eachLayer(function (layer) { if (layer instanceof L.Polygon) map.removeLayer(layer); });
}

function SaveEvent() {
    //event.preventDefault();
    if (!document.forms["formEvent"].reportValidity()) return;

    var obj = {};
    obj.properties = {};

    obj.id = getUIValue("txtEventID");
    obj.parcelId = ifEmpty(getUIValue("cboEventParcel"));
    obj.dat = ifEmpty(getUIValue("cboEventDats"));
    obj.eventStart = ifEmpty(getUIValue("txtEventDate1"));
    obj.eventEnd = ifEmpty(getUIValue("txtEventDate2"));
    obj.duration = ifEmpty(getUIValue("txtEventDur"));
    obj.type = ifEmpty(getUIValue("cboEventType"));
    obj.crop = ifEmpty(getUIValue("txtEventCrop"));
    obj.variety = ifEmpty(getUIValue("txtEventVar"));
    obj.comments = ifEmpty(getUIValue("txtEventComm"));
    obj.properties.metric = ifEmpty(getUIValue("txtEventSubstance"));
    obj.properties.target = ifEmpty(getUIValue("txtEventTarget"));
    obj.properties.productName = ifEmpty(getUIValue("txtEventProduct"));
    obj.properties.stage = ifEmpty(getUIValue("txtEventStage"));
    obj.properties.amount = ifEmpty(getUIValue("txtEventAmount"));
    obj.properties.unit = ifEmpty(getUIValue("cboEventAmountUnit"));
    obj.properties.unitRef = ifEmpty(getUIValue("cboEventAmountUnitRef"));
    obj.properties.fuelConsumption = ifEmpty(getUIValue("txtEventFuelConsum"));
    obj.properties.fuelType = ifEmpty(getUIValue("cboEventFuelType"));
    obj.properties.fuelUnit = ifEmpty(getUIValue("cboEventFuelUnit"));
    obj.properties.fuelUnitRef = ifEmpty(getUIValue("cboEventFuelUnitRef"));

    var par = {
        "path": "/event",
        "method": obj.id === "" ? "PUT" : "POST",
        "data": obj.id === "" ? [obj] : obj
    };

    CallAPI(apiURL, par, (data) => { CloseModal('modalEvent'); LoadEvents(); }, "eventEditErr", "btnSaveEvent");
}

function SaveMeteo() {
    //event.preventDefault();
    if (!document.forms["formMeteo"].reportValidity()) return;

    var obj = {};
    obj.properties = {};

    obj.id = getUIValue("txtMeteoID");
    obj.parcelId = ifEmpty(getUIValue("cboMeteoParcel"));
    obj.timestamp = ifEmpty(getUIValue("txtMeteoDate"));
    obj.temperature = ifEmpty(getUIValue("txtMeteoTemperature"));
    obj.humidity = ifEmpty(getUIValue("txtMeteoHumidity"));
    obj.windstrength = ifEmpty(getUIValue("txtMeteoWindstrength")); 
    obj.leafwetness = ifEmpty(getUIValue("txtMeteoLeafwetness"));

    var par = {
        "path": "/measurement",
        "method": obj.id === "" ? "PUT" : "POST",
        "data": obj.id === "" ? [obj] : obj
    };

    CallAPI(apiURL, par, (data) => { CloseModal('modalMeteo'); LoadMeteos(); }, "meteoEditErr", "btnSaveMeteo");
}

function SaveParcel() {
    //event.preventDefault();
    if (!document.forms["formParcel"].reportValidity()) return;

    var obj = {};
    obj.id = getUIValue("txtParcelID");
    obj.name = ifEmpty(getUIValue("txtParcelName"));
    obj.polygon = ifEmpty(getUIValue("txtParcelPoly"));
    obj.user = "";

    var par = {
        "path": "/parcel",
        "method": obj.id === "" ? "PUT" : "POST",
        "data": obj
    };

    CallAPI(apiURL, par, (data) => { CloseModal('modalParcel'); LoadParcelsAll(); }, "parcelEditErr", "btnSaveParcel");
}

function LoadEnumsData() {
    var par;
    var v = "value";

    par = {
        "path": "/parametrics/types/",
        "data": { "type": "" }
    };

    par.data.type = "EventType";
    CallAPI(apiURL, par, (data) => { PopulateSelect("eventType", data, v, "", true); PopulateSelect("resultEventType", data, v, ""); PopulateSelect("cboEventType", data, v, "") }, "", "");
    par.data.type = "FuelType";
    CallAPI(apiURL, par, (data) => { PopulateSelect("cboEventFuelType", data, v, "", true); }, "", "");
    par.data.type = "Unit";
    CallAPI(apiURL, par, (data) => { PopulateSelect("cboEventAmountUnit", data, v, "", true); PopulateSelect("cboEventFuelUnit", data, v, "", true); }, "", "");
    par.data.type = "UnitReference";
    CallAPI(apiURL, par, (data) => { PopulateSelect("cboEventAmountUnitRef", data, v, "", true); PopulateSelect("cboEventFuelUnitRef", data, v, "", true); }, "", "");

}

function LoadParcelsAll() {
    var par = {
        "path": "/parcel/user/",
        "data": { "id": "" }
    };

    CallAPI(apiURL, par, (data) => { dataParcels = data; LoadParcelDropDowns(); PopulateParcelList(data); LoadEvents(); LoadMeteos(); }, "parcelListBodyErr", "", "");
}

function LoadDATsAll() {
    var par = {
        "path": "/dats",
        "data": {}
    };

    CallAPI(apiURL, par, (data) => { dataDATs = data["@graph"]; FixDATids(dataDATs); PopulateSelect("parcelDAT", dataDATs, "rdfs:label", "id", true); PopulateSelect("cboEventDats", dataDATs, "rdfs:label", "id", true);}, "", "");
}

function LoadParcelsFilter() {

    var dat = getUIValue("parcelDAT");

    if (dat) {
        var par = {
            "path": "/parcel/dat",
            "method": "GET",
            "data": { "dat": dat }
        };

        CallAPI(apiURL, par, (data) => { PopulateParcelList(data); }, "eventListBodyErr", "", "");
    }
    else
        PopulateParcelList(dataParcels);

}

function LoadParcelDropDowns() {
    const propName = "name";
    const propValue = "uid";

    PopulateSelect("eventParcelName", dataParcels, propName, propValue, true);
    PopulateSelect("meteoParcelName", dataParcels, propName, propValue, false);
    PopulateSelect("cboEventParcel", dataParcels, propName, propValue, true);
    PopulateSelect("cboMeteoParcel", dataParcels, propName, propValue, true);
    PopulateSelect("cboImportParcel", dataParcels, propName, propValue, false);

    PopulateMultiSelect("result1Parcel1", dataParcels, propName, propValue);
    PopulateMultiSelect("result1Parcel2", dataParcels, propName, propValue);

    PopulateSelect("result2Parcel1", dataParcels, propName, propValue, true);
    PopulateSelect("result2Parcel2", dataParcels, propName, propValue, true);
}

function LoadEvents() {
    var par = {
        "path": "/event",
        "method": "GET",
        "data": {}
    };

    par.data.parcelid = getUIValue("eventParcelName") ? getUIValue("eventParcelName") : JoinProp(dataParcels, "uid");
    if (getUIValue("eventType")) par.data.event_type = getUIValue("eventType");
    if (getUIValue("eventDate1")) par.data.fromdate = getUIValue("eventDate1");
    if (getUIValue("eventDate2")) par.data.todate = getUIValue("eventDate2");

    CallAPI(apiURL, par, (data) => { PopulateEventList(data); }, "eventListBodyErr", "");
}

function LoadMeteos() {
    var par = {
        "path": "/measurement",
        "method": "GET",
        "data": {}
    };

    par.data.parcelId = getUIValue("meteoParcelName") ? getUIValue("meteoParcelName") : JoinProp(dataParcels, "uid");
    if (getUIValue("meteoDate1")) par.data.fromdate = getUIValue("meteoDate1");
    if (getUIValue("meteoDate2")) par.data.todate = getUIValue("meteoDate2");

    CallAPI(apiURL, par, (data) => { PopulateMeteoList(data); }, "meteoListBodyErr", "");
}

function PopulateParcelList(data) {
    var e = document.getElementById("parcelListBody");
    var html = "";

    data.forEach((item) => {
        html += 
    `<tr>
        <td>${item.name}</td>
        <td>${item.ektaria}</td>
        <td>${item.country}</td>
        <td>${item.county}</td>
        <td class="text-end">
            <button type="button" class="btn btn-success qf-bnt-sm" onclick="LoadParcel('${item.uid}')"><i class="ri-edit-line"></i></button>
            <button type="button" class="btn btn-warning qf-bnt-sm" onclick="DeleteParcel('${item.uid}')"><i class="ri-delete-bin-2-line"></i></button>
        </td>
    </tr>`
    }); 

    if (html === "") html = "(no parcels found)";

    e.innerHTML = html;
}

function PopulateEventList(data) {
    var e = document.getElementById("eventListBody");
    var html = "";

    data.forEach((item) => {
        html += 
    `<tr>
        <td>${GetParcelName(item.parcelId)}</td>
        <td>${item.type}</td>
        <td>${DateHM(item.eventStart)}</td>
        <td>${DateHM(item.eventEnd)}</td>
        <td>${item.metric}</td>
        <td class="text-end">
            <button type="button" class="btn btn-success qf-bnt-sm" onclick="LoadEvent('${item.id}')"><i class="ri-edit-line"></i></button>
            <button type="button" class="btn btn-warning qf-bnt-sm" onclick="DeleteEvent('${item.id}')"><i class="ri-delete-bin-2-line"></i></button>
        </td>
    </tr>`
    }); 

    if (html === "") html = "(no events found)";

    e.innerHTML = html;
}

function PopulateMeteoList(data) {
    var e = document.getElementById("meteoListBody");
    var html = "";

    data.forEach((item) => {
        html += 
    `<tr>
        <td>${DateHM(item.timestamp)}</td>
        <td>${item.temperature}</td>
        <td>${item.humidity}</td>
        <td>${item.windstrength}</td>
        <td>${item.leafwetness}</td>
        <td>${GetParcelName(item.parcelId)}</td>
        <td class="text-end">
            <button type="button" class="btn btn-success qf-bnt-sm" onclick="LoadMeteo('${item.id}')"><i class="ri-edit-line"></i></button>
            <button type="button" class="btn btn-warning qf-bnt-sm" onclick="DeleteMeteo('${item.id}')"><i class="ri-delete-bin-2-line"></i></button>
        </td>
    </tr>`
    }); 

    if (html === "") html = "(no measurements found)";

    e.innerHTML = html;
}

function GetParcelName(id) {
    return FindByProp(dataParcels, "uid", id, "name");
}

function OpenParcel(data) {

    if (!(data instanceof Object)) {
        data = {};
        data.uid = "";
        data.name = "";
        data.polygon = "";
    }

    setUIValue("txtParcelID", data.uid);
    setUIValue("txtParcelName", data.name);
    setUIValue("txtParcelPoly", data.polygon);

    document.getElementById("modalParcelTitle").innerText = (data.uid) ? "Edit Parcel" : "New Parcel";
    document.getElementById("parcelEditErr").innerHTML = '<span class="text-warning">All fields are required</span>';

    ShowModal('modalParcel');
    SetMap();
}

function LoadParcel(id) {
    var par = {
        "path": "/parcel/ids/",
        "data": { "id": id } 
    };

    CallAPI(apiURL, par, (data) => { OpenParcel(data[0]); }, "", "", "");
}

function OpenEvent(data) {

    if (!(data instanceof Object)) {
        data = {};

        data.id = "";
        data.parcelId = "";
        data.dat = "";
        data.eventStart = "";
        data.eventEnd = "";
        data.duration = "";
        data.type = "";
        data.crop = "";
        data.variety = "";
        data.comments = "";
        data.metric = ""; //properties.
        data.target = "";
        data.productName = "";
        data.stage = "";
        data.amount = "";
        data.unit = "";
        data.unitRef = "";
        data.fuelConsumption = "";
        data.fuelType = "";
        data.fuelUnit = "";
        data.fuelUnitRef = "";
    }

    setUIValue("txtEventID", data.id);
    setUIValue("cboEventParcel", data.parcelId);
    setUIValue("cboEventDats", data.dat);
    setUIValue("txtEventDate1", ifNull(data.eventStart).substring(0, 16));
    setUIValue("txtEventDate2", ifNull(data.eventEnd).substring(0, 16));
    setUIValue("txtEventDur", data.duration);
    setUIValue("cboEventType", data.type);
    setUIValue("txtEventCrop", data.crop);
    setUIValue("txtEventVar", data.variety);
    setUIValue("txtEventComm", data.comments);
    setUIValue("txtEventSubstance", data.metric); //properties.
    setUIValue("txtEventTarget", data.target);
    setUIValue("txtEventProduct", data.productName);
    setUIValue("txtEventStage", data.stage);
    setUIValue("txtEventAmount", data.amount);
    setUIValue("cboEventAmountUnit", data.unit);
    setUIValue("cboEventAmountUnitRef", data.unitRef);
    setUIValue("txtEventFuelConsum", data.fuelConsumption);
    setUIValue("cboEventFuelType", data.fuelType);
    setUIValue("cboEventFuelUnit", data.fuelUnit);
    setUIValue("cboEventFuelUnitRef", data.fuelUnitRef);

    document.getElementById("cboEventType").disabled = data.id;
    document.getElementById("cboEventParcel").disabled = data.id;

    document.getElementById("modalEventTitle").innerText = (data.id) ? "Edit Event" : "New Event";
    document.getElementById("eventEditErr").innerHTML = '<span class="text-warning">Fields marked with * are required</span>';

    ShowModal('modalEvent');
}

function OpenMeteo(data) {

    if (!(data instanceof Object)) {
        data = {};

        data.id = "";
        data.parcelId = "";
        data.timestamp = "";
        data.temperature = "";
        data.humidity = "";
        data.windstrength = "";
        data.leafwetness = "";
    }

    setUIValue("txtMeteoID", data.id);
    setUIValue("cboMeteoParcel", data.parcelId);
    setUIValue("txtMeteoDate", data.timestamp.substring(0, 16));
    setUIValue("txtMeteoTemperature", data.temperature);
    setUIValue("txtMeteoHumidity", data.humidity);
    setUIValue("txtMeteoWindstrength", data.windstrength);
    setUIValue("txtMeteoLeafwetness", data.leafwetness);

    document.getElementById("cboMeteoParcel").disabled = data.id;

    document.getElementById("modalMeteoTitle").innerText = (data.id) ? "Edit Measurement" : "New Measurement";
    document.getElementById("meteoEditErr").innerHTML = '<span class="text-warning">All fields are required</span>';

    ShowModal('modalMeteo');
}

function LoadEvent(id) {
    var par = {
        "path": "/event/id/",
        "data": { "id": id } 
    };

    CallAPI(apiURL, par, (data) => { OpenEvent(data[0]); }, "", "", "");
}

function LoadMeteo(id) {
    var par = {
        "path": "/measurement/id/",
        "data": { "id": id } 
    };

    CallAPI(apiURL, par, (data) => { OpenMeteo(data[0]); }, "", "", "");
}

function DeleteParcel(id) {

    if (!confirm("Click OK to delete parcel")) return;

    var par = {
        "path": "/parcel",
        "method": "DELETE",
        "data": { "id": id } 
    };

    CallAPI(apiURL, par, (data) => { LoadParcelsAll(); }, "", "", "");
}

function DeleteEvent(id) {

    if (!confirm("Click OK to delete event")) return;

    var par = {
        "path": "/event",
        "method": "DELETE",
        "data": { "id": id } 
    };

    CallAPI(apiURL, par, (data) => { LoadEvents(); }, "", "", "");
}

function DeleteMeteo(id) {

    if (!confirm("Click OK to delete measurement")) return;

    var par = {
        "path": "/measurement",
        "method": "DELETE",
        "data": { "id": id } 
    };

    CallAPI(apiURL, par, (data) => { LoadMeteos(); }, "", "", "");
}

function LoadArrg() {
    //event.preventDefault();
    if (!document.forms["formResult1"].reportValidity()) return;

    var obj, par;

    obj = {};
    obj.parcelid = getUIValue("result1Parcel1");
    if (obj.parcelid) {
        obj.aggregation_function = getUIValue("resultAggr");
        obj.evt_type = getUIValue("resultEventType");
        if (getUIValue("result1Date11")) obj.fromdate = getUIValue("result1Date11");
        if (getUIValue("result1Date12")) obj.todate = getUIValue("result1Date12");

        par = {
            "path": "aggr",
            "data": obj
        };

        CallAPI(apiURL, par, (data) => { PopulateResult1List(data, "result1ListBody1"); PopulateDATs("result1ListDATs1", data.dat); }, "result1ListBodyErr1", "btnShowResult1");
    }
    else {
        document.getElementById("result1ListBody1").innerHTML = "";
        document.getElementById("result1ListDATs1").innerHTML = "";
    }

    obj = {};
    obj.parcelid = getUIValue("result1Parcel2");
    if (obj.parcelid) {
        obj.aggregation_function = getUIValue("resultAggr");
        obj.evt_type = getUIValue("resultEventType");
        if (getUIValue("result1Date21")) obj.fromdate = getUIValue("result1Date21");
        if (getUIValue("result1Date22")) obj.todate = getUIValue("result1Date22");

        par = {
            "path": "aggr",
            "data": obj
        };

        CallAPI(apiURL, par, (data) => { PopulateResult1List(data, "result1ListBody2"); PopulateDATs("result1ListDATs2", data.dat); }, "result1ListBodyErr2", "");
    }
    else {
        document.getElementById("result1ListBody2").innerHTML = "";
        document.getElementById("result1ListDATs2").innerHTML = "";
    }

}

function LoadChill() {
    //event.preventDefault();
    if (!document.forms["formResult2"].reportValidity()) return;

    var obj, par;

    obj = {};
    obj.parcelid = getUIValue("result2Parcel1");
    if (obj.parcelid) {
        if (getUIValue("result2Date11")) obj.fromdate = getUIValue("result2Date11");
        if (getUIValue("result2Date12")) obj.todate = getUIValue("result2Date12");
        if (getUIValue("resultLow")) obj.chilling_low_threshold = getUIValue("resultLow");
        if (getUIValue("resultHigh")) obj.chilling_high_threshold = getUIValue("resultHigh");

        par = {
            "path": "chill",
            "data": obj
        };

        CallAPI(apiURL, par, (data) => { PopulateResult2List(data, "result2Chart1"); }, "result2ListBodyErr1", "btnShowResult2");
    }
    else {
        document.getElementById("result2Chart1").innerHTML = "";
    }

    obj = {};
    obj.parcelid = getUIValue("result2Parcel2");
    if (obj.parcelid) {
        if (getUIValue("result2Date21")) obj.fromdate = getUIValue("result2Date21");
        if (getUIValue("result2Date22")) obj.todate = getUIValue("result2Date22");
        if (getUIValue("resultLow")) obj.chilling_low_threshold = getUIValue("resultLow");
        if (getUIValue("resultHigh")) obj.chilling_high_threshold = getUIValue("resultHigh");

        par = {
            "path": "chill",
            "data": obj
        };

        CallAPI(apiURL, par, (data) => { PopulateResult2List(data, "result2Chart2"); }, "result2ListBodyErr2", "");
    }
    else {
        document.getElementById("result2Chart2").innerHTML = "";
    }

}

function ResultTypeChange() {
    var t = getUIValue("resultType");
    document.getElementById('formResult1').hidden = t == 2;
    document.getElementById('divResult1').hidden = t == 2;
    document.getElementById('formResult2').hidden = t == 1;
    document.getElementById('divResult2').hidden = t == 1;
}

function PopulateResult1List(data, ctrl) {
    var e = document.getElementById(ctrl);
    var html = "";

    data.info.rank.forEach((item) => {
        html +=
   `<tr>
        <td>${data.metric[item].metric}</td>
        <td class="text-end">${data.metric[item].total}</td>
        <td class="text-end">${data.metric[item].unit}</td>
        <td class="text-end">${data.metric[item].per_hectar}</td>
        <td class="text-end">${data.metric[item].total_hect}</td>
    </tr>`
    });

    if (html === "") html = "(no data)";

    e.innerHTML = html;
}

function PopulateResult2List(data, ctrl) {
    var el = document.getElementById(ctrl);
    var serie = [];
    var m = 0;

    data.data.forEach((item) => {
        serie.push({ x: new Date(item.timestamp), y: item.index });
        m = Math.max(m, item.index);
    });

    var options = {
        series: [{
            name: 'Index',
            data: serie
        }],
        chart: {
            id: ctrl,
            type: 'line',
            stacked: false,
            height: 350,
            zoom: {
                type: 'x',
                enabled: true,
                autoScaleYaxis: true
            },
            toolbar: {
                autoSelected: 'pan'
            }
        },
        dataLabels: {
            enabled: false
        },
        markers: {
            size: 5,
        },
        title: {
            text: 'Chilling hours',
            align: 'left'
        },
        yaxis: {
            labels: {
                formatter: function (val) {
                    return val.toFixed(1);
                }
            },
            title: {
                text: 'index'
            },
            max: ++m,
        },
        xaxis: {
            type: 'datetime',
        },
        tooltip: {
            shared: false,
            y: {
                formatter: function (val) {
                    return val;
                }
            },
            x: {
                formatter: function (val) {
                    return DateHM((new Date(val)).toISOString());
                }
            }
        }
    };

    if (Apex._chartInstances) Apex._chartInstances.forEach((c) => { if (c.id == ctrl) c.chart.destroy() });

    if (serie.length > 0) {
        el.innerHTML = "";
        var chart = new ApexCharts(el, options);
        chart.render();
    } else
        el.innerHTML = "(no data)";

}

function OpenImport(t) {
    if (t == "parcel")
        document.getElementById("modalImportTitle").innerText = "Import Parcels";
    else if (t == "event")
        document.getElementById("modalImportTitle").innerText = "Import Events";
    else if (t == "meteo")
        document.getElementById("modalImportTitle").innerText = "Import Measurements";

    document.getElementById("divImportParcel").hidden = t == "parcel";
    document.getElementById("importErr").innerHTML = '';
    document.getElementById("importObjType").value = t;
    document.getElementById("inputFile").value = '';
    ShowModal('modalImport');
}

function SaveImport() {
    const f = document.forms["formImport"];
    if (!f.reportValidity()) return;

    const payload = new FormData(f);
    const t = document.getElementById("importObjType").value;

    if (t == "parcel")
        CallUploadAPI(payload, (data) => { CloseModal('modalImport'); LoadParcelsAll(); }, "importErr", "btnImport");
    else if (t == "event")
        CallUploadAPI(payload, (data) => { CloseModal('modalImport'); LoadEvents(); }, "importErr", "btnImport");
    else if (t == "meteo")
        CallUploadAPI(payload, (data) => { CloseModal('modalImport'); LoadMeteos(); }, "importErr", "btnImport");

}

function CallUploadAPI(par, funcSuccess, errContainer, startButton) {

    var url = baseURL + "upload.php";

    if (startButton != "") setButtonState(startButton, true);

    fetch(url, { method: 'POST', mode: 'cors', body: par })
        .then(function (response) {
            if (response.ok)
                return response.json();
            else
                return response.text()
        })
        .then(function (data) {
            console.log(data);
            funcSuccess(data);

        //    if (data instanceof Object) {
        //        if (funcSuccess instanceof Function) funcSuccess(data);
        //    }
        //    else
        //        throw new Error(data.substring(0, 100));
        })
        .catch(function (error) {
            if (errContainer != "") showErr(errContainer, error);
            console.log(error);
        })
        .finally(function () {
            if (startButton != "") setButtonState(startButton, false);
        });
}

function FixDATids(data) {

    data.forEach((item) => { item.id = item["@id"].replace("qfdata:", "https://quantifarm.eu/data/"); });

}

function PopulateDATs(ctrl, dats) {
    var div = document.getElementById(ctrl);
    var s = ""; 

    for (var i = 0; i < dataDATs.length; i++) {
        if (dats.includes(dataDATs[i].id)) {
            s += `<p><a href="#" onclick="LoadDATDetals('${dataDATs[i]["@id"].replace("qfdata:", "https://quantifarm.eu/data/")}'); return false; "><i class="bi bi-gear me-2"></i>${dataDATs[i]["rdfs:label"]}</a></p>`;
        }
    }

    div.innerHTML = (s === "") ? '<p class="fw-bold">No DATSs used</p>' : '<p class="fw-bold">DATSs used:</p>';
    div.innerHTML += s;
}

