const tool_key = "tool1";
const apiURL = baseURL + "tool1.php";

const pageSize = 12;
var currentN = 0;
var cardData;
var last_sidebar_filters = {};

document.addEventListener("DOMContentLoaded", setSidebar(tool_key));

LoadEnumsData();


function LoadEnumsData() {
    var par = {
        "path": "/initialize_filter_list/",
        "data": {}
    };

    showSpinner("divResultInfo");

    CallAPI(apiURL, par, (data) => { PopulateEnumsData(data["filter_list"]); }, "divResultInfo", "");
}

function PopulateEnumsData(data) {
    const v1 = "option";

    UpdateAllCheckListData(data);
    PopulateSelect("farmCountry", data.countries, v1, "", true);
    PopulateSelect("farmLanguage", data.languages, v1, "", true);
    document.getElementById("divResultInfo").innerHTML = "";
}

function UpdateAllCheckListData(data) {
    const v1 = "option";
    const v2 = "count";

    PrepareCheckList(data.agricultural_sectors, last_sidebar_filters.agricultural_sectors);
    PrepareCheckList(data.functionalities, last_sidebar_filters.functionalities);
    PrepareCheckList(data.benefits, last_sidebar_filters.benefits);
    PrepareCheckList(data.digital_forms, last_sidebar_filters.digital_forms);
    PrepareCheckList(data.countries, last_sidebar_filters.countries);
    PrepareCheckList(data.languages, last_sidebar_filters.languages);
    PrepareCheckList(data.cost_structures, last_sidebar_filters.cost_structures);

    PopulateCheckList("filterSector", data.agricultural_sectors, v1, v2);
    PopulateCheckList("filterFunctionality", data.functionalities, v1, v2);
    PopulateCheckList("filterBenefits", data.benefits, v1, v2);
    PopulateCheckList("filterDigitalform", data.digital_forms, v1, v2);
    PopulateCheckList("filterCountry", data.countries, v1, v2);
    PopulateCheckList("filterLanguage", data.languages, v1, v2);
    PopulateCheckList("filterCost", data.cost_structures, v1, v2);

}

function PrepareCheckList(data, filter) {
    if (!data) return;

    //set the last checked
    data.forEach((item) => {
        if (filter && filter.indexOf(item.option) >= 0) item.checked = true;
    });

    //move checked on top
    data.sort(function (a, b) { return (a.checked ? 0 : 1) - (b.checked ? 0 : 1) });
}

function Recommend() {
    var par = {
        "path": "/recommended_dat_list/",
        "data": {},
        "q": {},
    };

    par.q.algorithm_type = "exact";
    CollectFarmerFilters(par.data);
    CollectSidebarFilters(par.data);
    CollectSearchbarFilters(par.data);

    last_sidebar_filters = par.data.sidebar_filters ? par.data.sidebar_filters : {};

    console.log("POST /recommended_dat_list/ body:", par.data);

    CallAPI(apiURL, par, (data) => {
        console.log("return:", data);

        cardData = data.recommendation_list;
        if (!cardData) cardData = [];
        currentN = 0;
        PopulateDATs();
        UpdateAllCheckListData(data.filter_list);
    }, "divResultInfo", "btnRecommend");

}

function CollectFarmerFilters(obj) {
    var v;
    var o = obj.farmer_profile = {};

    v = getUIValue("farmFarmType");
    if (v) o.farm_type = { "value": v, "requireExactMatch": true };
    v = getUIValue("farmCountry");
    if (v) o.country = { "value": v, "requireExactMatch": true };
    v = getUIValue("farmLanguage");
    if (v) o.language = [{ "value": v, "requireExactMatch": true }];

    if (Object.keys(o).length == 0) delete obj.farmer_profile;
}

function CollectSidebarFilters(obj) {
    var arr;
    var o = obj.sidebar_filters = {};

    arr = getCheckListValues("filterSector");
    if (arr.length > 0) o.agricultural_sectors = arr;
    arr = getCheckListValues("filterFunctionality");
    if (arr.length > 0) o.functionalities = arr;
    arr = getCheckListValues("filterBenefits");
    if (arr.length > 0) o.benefits = arr;
    arr = getCheckListValues("filterDigitalform");
    if (arr.length > 0) o.digital_forms = arr;
    arr = getCheckListValues("filterLanguage");
    if (arr.length > 0) o.languages = arr;
    arr = getCheckListValues("filterCountry");
    if (arr.length > 0) o.countries = arr;
    arr = getCheckListValues("filterCost");
    if (arr.length > 0) o.cost_structures = arr;

    if (Object.keys(o).length == 0) delete obj.sidebar_filters;
}

function CollectSearchbarFilters(obj) {
    var v;

    v = getUIValue("txtSearchText");
    if (v) {
        var o = obj.searchbar_filter = {};
        o["query_string"] = v;
        o["search_description"] = true;
        o["search_title"] = true;
        o["search_keywords"] = true;
    }
}

function PopulateDATs() {
    var div = document.getElementById("divResult");
    var divInfo = document.getElementById("divResultInfo");
    var divMore = document.getElementById("divResultMore");

    if (currentN == 0) {
        div.innerHTML = "";
        divInfo.innerHTML = "";
        divMore.innerHTML = "";
        divInfo.appendChild(CreateTotal(cardData.length));
    }
    else
        divMore.innerHTML = "";

    var el;
    var j = 0;
    for (var i = currentN; i < cardData.length; i++) {

        j++;
        if (j > pageSize) {
            el = CreateLoadMore();
            divMore.appendChild(el);
            break;
        }

        item = cardData[i];
        el = CreateCard(item.description.ID, item.description.name, getObjPropVal(item.filterable_properties, "agricultural_sectors"), getObjPropVal(item.filterable_properties, "functionalities"), getObjPropVal(item.filterable_properties, "cost_structures"));
        div.appendChild(el);
    }

    currentN += --j;
}

function CreateCard(id, label, sector, functionality, cost) {
    var card = document.createElement("div");
    card.classList.add("col");

    card.innerHTML = `
<div class="card mb-0 h-100">
    <div class="card-body">
        <h5 class="card-title"><a href="#" onclick="return LoadDATDetals('${id}');"><i class="bi bi-gear me-2"></i>${label}</a></h5>
        <p>
            <i class="bi bi-caret-right-fill me-1"></i><b>Agricultural Sector</b><br>
            <small>${sector}</small>
        </p>
        <p>
            <i class="bi bi-caret-right-fill me-1"></i><b>Functionality</b><br>
            <small>${functionality}</small>
        </p>
        <p>
            <i class="bi bi-caret-right-fill me-1"></i><b>Cost structure</b><br>
            ${cost}
        </p>
    </div>
</div>
`;

    return card;
}

function CreateLoadMore() {
    var el = document.createElement("div");

    el.innerHTML = `<div class="d-flex justify-content-center" id="buttonLoadMore">
 <button type="button" class="btn btn-primary rounded-pill" onclick="PopulateDATs()">
 <i class="bi bi-arrow-down"></i>&nbsp;&nbsp;&nbsp; ${uistrings["load_more"]} &nbsp;&nbsp;&nbsp;<i class="bi bi-arrow-down"></i></button>
 </div>`;

    return el;
}

function CreateTotal(n) {
    var el = document.createElement("div");

    el.innerHTML = `<div class="d-flex justify-content-left">${n} DATSs found
 </div>`;

    return el;
}

function OnKeyPressSearch(event) {
    if (event.key === "Enter") Recommend();
}

function ClearFilters() {

    document.getElementById("divFilters").querySelectorAll("input[type='checkbox']").forEach((el) => {
        el.checked = false;
    });

    return false;
}
