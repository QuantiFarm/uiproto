const searchParams = new URLSearchParams(window.location.search);
const n = Math.max(Math.min(parseInt(searchParams.get('n')) || 1, 30), 1);
const nn = n.toString().padStart(2, '0');
const tc = testCaseData[n - 1];

document.getElementById('tcTitle').innerHTML += " " + n;
document.getElementById('tcSector').innerHTML = tc.sector;
document.getElementById('tcCrop').innerHTML = tc.crop;
document.getElementById('tcCountry').innerHTML = tc.country;
document.getElementById('tcDAT').innerHTML = `<a href="#" onclick="LoadDATDetals('${tc.dats[0].id}');return false;">${tc.dats[0].name}</a>`;
    
var t = document.getElementById('tcTable');
for (var i = 1; i < tc.dats.length; i++) {
    t.innerHTML += `<tr><td class="text-nowrap pe-2 text-end w-50"></td><td class="text-start fw-bold w-50"><a href="#" onclick="LoadDATDetals('${tc.dats[i].id}');return false;">${tc.dats[i].name}</a></td></tr>`;
}

document.getElementById('a2023').href = `downloads/TC${nn}-2023-Assessment-report.pdf`;
document.getElementById('a2024').href = `downloads/TC${nn}-2024-Assessment-report.pdf`;
document.getElementById('a2025').href = `downloads/TC${nn}-2025-Assessment-report.pdf`;

document.getElementById('div2023-1').innerHTML = `<img src="assets/img/TC${nn}-2023-1.png" class="img-fluid" onerror="NoImg(2023)"/>`;
document.getElementById('div2023-2').innerHTML = `<img src="assets/img/TC${nn}-2023-2.png" class="img-fluid" onerror="NoImg(2023)"/>`;
document.getElementById('div2023-3').innerHTML = `<img src="assets/img/TC${nn}-2023-3.png" class="img-fluid" onerror="NoImg(2023)"/>`;

document.getElementById('div2024-1').innerHTML = `<img src="assets/img/TC${nn}-2024-1.png" class="img-fluid" onerror="NoImg(2024)"/>`;
document.getElementById('div2024-2').innerHTML = `<img src="assets/img/TC${nn}-2024-2.png" class="img-fluid" onerror="NoImg(2024)"/>`;
document.getElementById('div2024-3').innerHTML = `<img src="assets/img/TC${nn}-2024-3.png" class="img-fluid" onerror="NoImg(2024)"/>`;

document.getElementById('div2025-1').innerHTML = `<img src="assets/img/TC${nn}-2025-1.png" class="img-fluid" onerror="NoImg(2025)"/>`;
document.getElementById('div2025-2').innerHTML = `<img src="assets/img/TC${nn}-2025-2.png" class="img-fluid" onerror="NoImg(2025)"/>`;
document.getElementById('div2025-3').innerHTML = `<img src="assets/img/TC${nn}-2025-3.png" class="img-fluid" onerror="NoImg(2025)"/>`;

//2023: exeption for TC12, TC29: no impacts were recorded. Only the Reports were shared.
if (n == 12 || n == 29) {
    document.getElementById('tabTab1').innerHTML = `<div class="row text-center mt-3"><h5 class="card-title lh-lg"><a href="downloads/TC${nn}-2023-Assessment-report.pdf"><i class="bi bi-download me-2"></i>Download the full assessment report</a></h5></div>`;
}


function NoImg(y) {
    document.getElementById('divNoData' + y).hidden = false;
    document.getElementById('divData' + y).hidden = true;
}

