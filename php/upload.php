<?php
header('Access-Control-Allow-Methods: POST');

require_once 'app.php';

session_start();
CheckLogin();

if($_FILES["inputFile"]["error"] == 2 )
	die ("File too big");

if($_FILES["inputFile"]["error"] == 2)
	die ("error: " . $_FILES["inputFile"]["error"]);

if($_FILES["inputFile"]["size"] > 1000000)
	die ("File too big");


$f = $_FILES["inputFile"]["tmp_name"];
$q = new stdClass();
$baseURL = 'http://localhost:8686';

switch ($_POST["objType"]) {
    case 'parcel':
		
		$q->username = $_SESSION['useremail'];
		$url = $baseURL . '/import/parcel?' . http_build_query($q);
		$res = UploadFile($url, $f, 'filedata');
        break;
    case 'event':

		$q->parcelid = $_POST["importParcel"];
		$url = $baseURL . '/import/event?' . http_build_query($q);
		$res = UploadFile($url, $f, 'filedata');
        break;
    case 'meteo':

		$q->parcelid = $_POST["importParcel"];
		$url = $baseURL . '/import/meteo?' . http_build_query($q);
		$res = UploadFile($url, $f, 'filedata');
        break;
    default: 

        http_response_code(400);
	    $res = 'unknown obj';
}

echo $res;

