<?php

function CheckLogin(){
    if (!isset($_SESSION['loggedin'])) {
        http_response_code(401);
        die ('not logged.');
    }
}

function CallAPI($method, $url, $data) {
     
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

    if ($_SERVER['HTTP_HOST'] == 'localhost') {
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    }
    
    if (!is_null($data)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    }

    $res = curl_exec($ch);

    $ec = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($ec == 0) $ec = 500;
    http_response_code($ec);

    if (curl_errno($ch)) {
        $res = 'curl error: ' . curl_errno($ch) . '> ' . curl_error($ch);
    }

    curl_close($ch);

    return $res;
}

function UploadFile($url, $file, $fieldName) {
    
    $f = new \CURLFile($file, 'text/csv');

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FAILONERROR, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, [$fieldName => $f]);

    if ($_SERVER['HTTP_HOST'] == 'localhost') {
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    }
    
    $res = curl_exec($ch);

    $ec = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($ec == 0) $ec = 500;
    http_response_code($ec);

    if (curl_errno($ch)) {
        $res = 'curl error: ' . curl_errno($ch) . '> ' . curl_error($ch);
    }

    curl_close($ch);

    return $res;
}


//send mail
//$to = "divaylo@gmail.com";
//$subject = "test php";
//$txt = "Hello world!";
//$headers = "From: quantifarmtoolkit@quantifarmtoolkit.eu";

//$x = mail($to,$subject,$txt,$headers);   // 1 - ok


