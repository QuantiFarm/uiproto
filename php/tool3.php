<?php
header('Access-Control-Allow-Headers: X-Requested-With, Authorization,Content-Type');
header('Access-Control-Allow-Methods: POST');
header('Content-Type: application/json');

require_once 'app.php';

session_start();
CheckLogin();

$baseURL = 'http://localhost:8686';

$req = json_decode(file_get_contents('php://input'));

$path = $req->path;
$data = $req->data;
if (property_exists($req,'method')) $method = $req->method;

switch ($path) {
    case '/parcel/ids/':
    case '/parcel/name/':
    case '/event/id/':
    case '/measurement/id/':
        
        $url = $baseURL . $path . urlencode($data->id);
        $res = CallAPI('GET', $url, null);

        break;
    case '/parcel/user/':
              
        $data->id = $_SESSION['useremail'];
        $url = $baseURL . $path . urlencode($data->id);
        $res = CallAPI('GET', $url, null);

        break;
    case '/event':
    case '/measurement':
        
        if($method == "GET" || $method == "DELETE") {
            $url = $baseURL . $path . '?' . http_build_query($data);
            $res = CallAPI($method, $url, null);
        }
        else if($method == "POST") {
            $url = $baseURL . $path . '?id=' . $data->id;
            $res = CallAPI($method, $url, $data);
        }
        else if($method == "PUT") {
            $url = $baseURL . $path;
            $res = CallAPI($method, $url, $data);
        }

        break;
    case '/parcel':

        $data->user = $_SESSION['useremail'];
        $url = $baseURL . $path . '?' . http_build_query($data);
        $res = CallAPI($method, $url, null);

        break;
    case '/parcel/dat':

        $url = $baseURL . $path . '?' . http_build_query($data);
        $res = CallAPI($method, $url, null);

        break;
    case 'aggr':
        $pid = $data->parcelid;
        $q = json_decode(json_encode($data), true);
        unset($q["parcelid"]);

        $url = $baseURL . '/parcels/' . urlencode($pid) . '/aggregations/' . '?' . http_build_query($q);
        $res = CallAPI("GET", $url, null);

        break;
    case 'chill':
        $pid = $data->parcelid;
        $q = json_decode(json_encode($data), true);
        unset($q["parcelid"]);

        $url = $baseURL . '/parcels/' . urlencode($pid) . '/chilling-days/' . '?' . http_build_query($q);
        $res = CallAPI("GET", $url, null);

        break;
    case '/parametrics/types/':
        
        $url = $baseURL . $path . urlencode($data->type);
        $res = CallAPI('GET', $url, null);

        break;
    case '/dats': 
        //todo: for now call Recomm-tool API
        $url = 'http://localhost:8000/dats/';
        $res = CallAPI('GET', $url, null);

        break;
    case '/datdetails': 
        //todo: for now call Recomm-tool API
        $url = 'http://localhost:8000/dats/' . '?' . http_build_query($data);
        $res = CallAPI('GET', $url, null);

        break;
    default: 

        http_response_code(400);
	    $res = 'unknown path: ' . $path;
}

echo $res;

