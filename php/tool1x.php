<?php
header('Access-Control-Allow-Headers: X-Requested-With, Authorization,Content-Type');
header('Access-Control-Allow-Methods: POST');
header('Content-Type: application/json');

require_once 'app.php';

//$baseURL = 'http://localhost:8001';
$baseURL = 'http://quantifarmtoolkit.eu:8001';

$req = json_decode(file_get_contents('php://input'));

$path = $req->path;
$data = $req->data;
if (property_exists($req,'method')) $method = $req->method;

switch ($path) {
    case '/show_main_dat/':

        $url = $baseURL . $path . '?' . http_build_query($data);
        $res = CallAPI('GET', $url, null);

        break;
    case '/initialize_filter_list/':
      
        $url = $baseURL . $path;
        $res = CallAPI('GET', $url, null);

        break;
    case '/profile_entries/':
      
        $url = $baseURL . $path;
        $res = CallAPI('GET', $url, null);

        break;
    case '/recommended_dat_list/':
        
        $url = $baseURL . $path . '?' . http_build_query($req->q);
        $res = CallAPI('POST', $url, $data);

        break;
    default: 

        http_response_code(400);
	    $res = 'unknown path: ' . $path;
}

echo $res;

